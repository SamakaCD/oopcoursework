package ua.iyps.oopcoursework.entity;

import java.io.File;
import java.util.List;

import ua.iyps.oopcoursework.utils.AudioFileListWriter;
import ua.iyps.oopcoursework.utils.AudioFilesListReader;

public class AudioFile implements Cloneable {

	/** Розміщення файлу на інтернет-ресурсі */
	public String url;

	/** Розширення файлу */
	public String extension;

	/** Тривалість аудіо-файлу у секундах */
	public Integer duration;

	/** Кодек файлу */
	public String codec;

	/** Визначає, чи містить даний файл текст для караоке */
	public boolean hasKaraokeText;

	/** Частота дискретизації */
	public Integer sampleRate;

	/** Бітрейт */
	public Integer bitrate;

	/** Плеєр для відкриття цього файлу */
	public String playerForOpen;

	/**
	 * Конструктор по-замовчуванню
	 */
	public AudioFile() {

	}

	/**
	 * Конструктор з параметрами
	 * @param extension розширення файлу
	 * @param duration тривалість
	 * @param codec кодек аудіо
	 * @param sampleRate частота дискретизації
	 * @param bitrate бітрейт аудіо
	 */
	public AudioFile(String extension, int duration, String codec, int sampleRate, int bitrate) {
		this.extension = extension;
		this.duration = duration;
		this.codec = codec;
		this.sampleRate = sampleRate;
		this.bitrate = bitrate;
	}

	/**
	 * Конструктор копіювання
	 * @param audioFile аудіофайл, з якого будуть братись дані для копіювання
	 */
	public AudioFile(AudioFile audioFile) {
		this.url = audioFile.url;
		this.extension = audioFile.extension;
		this.duration = audioFile.duration;
		this.codec = audioFile.codec;
		this.hasKaraokeText = audioFile.hasKaraokeText;
		this.sampleRate = audioFile.sampleRate;
		this.bitrate = audioFile.bitrate;
		this.playerForOpen = audioFile.playerForOpen;
	}

	/** Клонує аудіо-файл */
	@Override
	public AudioFile clone() {
		AudioFile copy = new AudioFile();
		copy.url = url;
		copy.extension = extension;
		copy.duration = duration;
		copy.codec = codec;
		copy.hasKaraokeText = hasKaraokeText;
		copy.sampleRate = sampleRate;
		copy.bitrate = bitrate;
		copy.playerForOpen = playerForOpen;
		return copy;
	}

	/**
	 * Читає список аудіофайлів з файлу
	 * @param file файл, звідки буде читатись список
	 * @return список аудіофайлів
	 * @throws Exception у випадку помилки зчитування (на приклад, якщо немає прав на читання)
	 */
	public static List<AudioFile> readFromFile(File file) throws Exception {
		AudioFilesListReader reader = new AudioFilesListReader();
		return reader.read(file);
	}

	/**
	 * Зберігає список аудіофайлів у файл
	 * @param file файл, куди будуть записуватись дані
	 * @param audioFiles список аудіофайлів
	 * @throws Exception у випадку помилки запису (на приклад, якщо немає прав на запису)
	 */
	public static void saveToFile(File file, List<AudioFile> audioFiles) throws Exception {
		AudioFileListWriter writer = new AudioFileListWriter();
		writer.write(file, audioFiles);
	}

	/**
	 * Порівнює два аудіофайли по їх частотах дискретизацї
	 */
	public static int compareBySampleRate(AudioFile file1, AudioFile file2) {
		if(file1.sampleRate == null) {
			return -1;
		}

		if(file2.sampleRate == null) {
			return -1;
		}

		int sampleRate1 = file1.sampleRate;
		int sampleRate2 = file2.sampleRate;

		if(sampleRate1 == sampleRate2) {
			return 0;
		} else if(sampleRate1 > sampleRate2) {
			return 1;
		} else {
			return -1;
		}
	}

}