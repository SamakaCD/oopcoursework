package ua.iyps.oopcoursework.presentation.common;

public interface BaseView {

	void showError(String message);
	void showError(int messageStringResId);

}