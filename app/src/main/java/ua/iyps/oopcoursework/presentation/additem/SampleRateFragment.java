package ua.iyps.oopcoursework.presentation.additem;

import android.text.TextUtils;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.utils.ValidatorHelper;

/**
 * Фрагмент, який відповідає за запис частоти дискретизації
 */
@Layout(R.layout.fragment_add_sample_rate)
public class SampleRateFragment extends BaseFragment implements ValidatorHelper.Validatable {

	@BindView(R.id.field)
	EditText editText;

	AddItemContract contract;
	AudioFile audioFile;

	public SampleRateFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ValidatorHelper.bind(editText, this);
		if(audioFile.sampleRate != null) {
			editText.setText(String.valueOf(audioFile.sampleRate));
		}
	}

	@Override
	public boolean validate() {
		String input = editText.getText().toString();
		try {
			Integer.parseInt(input);

			if(!TextUtils.isEmpty(input) && input.length() > 8) {
				return false;
			}

			return true;
		} catch(Exception e) {
		    e.printStackTrace();
		}

		return false;
	}

	@Override
	public void onValid() {
		audioFile.sampleRate = Integer.parseInt(editText.getText().toString());
		contract.addFragment(new BitrateFragment(contract));
		contract.setPageIndex(6);
	}

	@Override
	public void onError() {
		if(editText.getText().toString().length() > 8) {
			showError("Введена частота дискретизації надто велика.");
		} else {
			showError("Введена некоректра частота дискретизації, написання та повторіть спробу.");
		}
	}

	@Override
	public void onSilentLock() {
		contract.setPagingEnabled(false);
	}

	@Override
	public void onSilentUnlock() {
		contract.setPagingEnabled(true);
	}

	@OnClick(R.id.sampleRate1)
	void onSelectSampleRate1() {
		editText.setText("44100");
		editText.requestFocus();
	}

	@OnClick(R.id.sampleRate2)
	void onSelectSampleRate2() {
		editText.setText("8000");
		editText.requestFocus();
	}

	@OnClick(R.id.sampleRate3)
	void onSelectSampleRate3() {
		editText.setText("192000");
		editText.requestFocus();
	}

	@OnClick(R.id.sampleRate4)
	void onSelectSampleRate4() {
		editText.setText("11025");
		editText.requestFocus();
	}

	@OnClick(R.id.sampleRate5)
	void onSelectSampleRate5() {
		editText.setText("22050");
		editText.requestFocus();
	}

}