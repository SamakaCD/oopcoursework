package ua.iyps.oopcoursework.presentation.savefile;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import teamvoy.fragmentpico.FragmentHolder;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.di.Injector;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;
import ua.iyps.oopcoursework.presentation.common.Layout;

/**
 * Фрагмент збереження файлу
 */
@Layout(R.layout.fragment_save_file)
public class SaveFileFragment extends BaseFragment implements SaveFileView {

	/**
	 * Presenter фрагмента
	 */
	@Inject
	SaveFilePresenter presenter;

	/**
	 * Контейнер фрагмента
	 */
	@Inject
	FragmentHolder fragmentHolder;

	/**
	 * Відображення поточного шляху
	 */
	@BindView(R.id.currentFolder)
	TextView currentFolder;

	/**
	 * Список
	 */
	@BindView(R.id.recyclerView)
	RecyclerView recyclerView;

	/**
	 * Ім'я файлу
	 */
	@BindView(R.id.fileNameField)
	EditText fileNameEditText;

	/**
	 * Адаптер списку
	 */
	SaveFileListAdapter adapter;

	/**
	 * Викликається при створенні фрагменту
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		setupRecyclerView();
	}

	/**
	 * Викликається при запиті на відкріплення фрагмента
	 * @return true, якщо фрагмент можна відкріпити, інакше false
	 */
	@Override
	public boolean onRequestDetach() {
		return presenter.canExit();
	}

	/**
	 * Встановлює шлях до поточної папки
	 * @param path шлях до поточної папки
	 */
	@Override
	public void setCurrentFolder(String path) {
		currentFolder.setText(path);
	}

	/**
	 * Очищає список
	 */
	@Override
	public void clearList() {
		adapter.clear();
	}

	/**
	 * Додає пункт "угору"
	 */
	@Override
	public void addUp() {
		adapter.addUp();
	}

	/**
	 * Додає пункт "папка"
	 * @param folder папка, якій відповідатиме пункт
	 */
	@Override
	public void addFolder(File folder) {
		adapter.addFolder(folder);
	}

	/**
	 * Додає пункт "файл"
	 * @param file файл, якому відповідатиме пункт
	 */
	@Override
	public void addFile(File file) {
		adapter.addFile(file);
	}

	@Override
	public String getFileName() {
		return fileNameEditText.getText().toString();
	}

	@Override
	public void close() {
		fragmentHolder.remove(this);
	}

	@Override
	public void showError(String message) {
		Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
	}

	@OnClick(R.id.saveButton)
	void onSaveButtonClick() {
		presenter.onSaveButtonClick();
	}

	@Override
	protected void inject() {
		Injector.getAppComponent().inject(this);
	}

	@Override
	protected BasePresenter getPresenter() {
		return presenter;
	}

	/**
	 * Установлює параметри та адаптер списку
	 */
	private void setupRecyclerView() {
		LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
		adapter = new SaveFileListAdapter();
		adapter.setOnItemSelectedListener(presenter::onListItemSelected);
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setAdapter(adapter);
	}

}