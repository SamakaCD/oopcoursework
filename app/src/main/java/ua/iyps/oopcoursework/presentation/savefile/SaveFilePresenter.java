package ua.iyps.oopcoursework.presentation.savefile;

import android.text.TextUtils;

import com.annimon.stream.Stream;

import java.io.File;

import javax.inject.Inject;

import rx.functions.Action1;
import ua.iyps.oopcoursework.Constants;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;
import ua.iyps.oopcoursework.presentation.openfile.OpenFileListAdapter;

public class SaveFilePresenter extends BasePresenter<SaveFileView> {

	private static final String[] UNALLOWED_CHARS = { "\r", "\n", "\t", "\"", "?", "'", ":", "*", "|", "/", "\\", "<", ">" };

	private File currentFolder;

	@Inject
	public SaveFilePresenter() {

	}

	@Override
	protected void onStart() {
		super.onStart();
		goTo(new File("/sdcard/"));
	}

	/**
	 * Викликається, коли користувач вибрав пункт списку
	 * @param type тип пункту
	 * @param file об'єкт типу File, якщо пункт є файлом або папкою, інакше null
	 */
	public void onListItemSelected(OpenFileListAdapter.Item.Type type, File file) {
		switch (type) {
			case FOLDER:
				goTo(file);
				break;
			case UP:
				goTo(currentFolder.getParentFile());
				break;
		}
	}

	/**
	 * Викликається при натисканні користувачем кнопки "назад"
	 * @return true, якщо фрагмент можна закрити, інакше false
	 */
	public boolean canExit() {
		if(isRootFolder(currentFolder)) {
			return true;
		} else {
			goTo(currentFolder.getParentFile());
			return false;
		}
	}

	public void onSaveButtonClick() {
		String fileName = getView().getFileName();

		if(TextUtils.isEmpty(fileName)) {
			getView().showError("Ім'я файлу не може бути порожнім");
		}

		if(!isCorrectFileName(fileName)) {
			getView().showError("Введене некоректне ім'я файлу.");
			return;
		}

		if(checkCurrentFolderContainsFile(currentFolder, fileName)) {
			getView().showError("Поточна папка вже містить файл з такою назвою");
			return;
		}

		getView().close();
		File file = new File(currentFolder, fileName);
		((Action1<File>) getArgument(Constants.ARG_SAVE_FILE_CALLBACK)).call(file);
	}

	/**
	 * Переходить у папку
	 * @param folder папка, у яку необхідно перейти
	 */
	private void goTo(File folder) {
		getView().clearList();
		getView().setCurrentFolder(folder.getAbsolutePath());
		currentFolder = folder;

		// Якщо папка не корінна, додаємо пункт "угору"
		if(!isRootFolder(folder)) {
			getView().addUp();
		}

		// Додавання папок
		Stream.of(folder.listFiles())
				.filter(File::isDirectory)
				.filter(file -> !file.getName().startsWith("."))
				.sortBy(File::getName)
				.forEach(file -> getView().addFolder(file));

		// Додавання файлів
		Stream.of(folder.listFiles())
				.filter(File::isFile)
				.filter(file -> !file.getName().startsWith("."))
				.sortBy(File::getName)
				.forEach(file -> getView().addFile(file));
	}

	/**
	 * Перевіряє чи папка є корінною
	 * @param file папка для перевірки
	 * @return true, якщо папка корінна, інакше false
	 */
	private boolean isRootFolder(File file) {
		return file.getName().contains("sdcard");
	}

	private boolean isCorrectFileName(String fileName) {
		for(String symbol : UNALLOWED_CHARS) {
			if(fileName.contains(symbol)) {
				return false;
			}
		}
		return true;
	}

	private boolean checkCurrentFolderContainsFile(File folder, String fileName) {
		String[] folderFiles = folder.list();
		for(String folderFileName : folderFiles) {
			if(folderFileName.equals(fileName)) {
				return true;
			}
		}
		return false;
	}

}