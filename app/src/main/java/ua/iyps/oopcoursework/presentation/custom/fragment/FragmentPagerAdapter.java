package ua.iyps.oopcoursework.presentation.custom.fragment;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import teamvoy.fragmentpico.Fragment;
public class FragmentPagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

	private List<Fragment> fragments;
	private Context context;
	private int lastPagePosition;

	public FragmentPagerAdapter(Context context) {
		fragments = new ArrayList<>();
		this.context = context;
	}

	@Override
	public int getItemPosition(Object object) {
		int index = fragments.indexOf(object);
		if(index != -1) {
			return index;
		} else {
			return POSITION_NONE;
		}
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = fragments.get(position).getView();
		container.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView(fragments.get(position).getView());
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	public void addFragment(Fragment fragment) {
		if(checkContainsSameFramgent(fragment))
			return;

		fragment.updateContext(context);
		fragment.setupView();
		fragment.onCreate();
		fragment.onPostCreate();
		fragments.add(fragment);
		notifyDataSetChanged();
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		fragments.get(lastPagePosition).onHide();
		fragments.get(position).onShow();
		lastPagePosition = position;
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	private boolean checkContainsSameFramgent(Fragment fragment) {
		for(Fragment installedFragment : fragments) {
			if(installedFragment.getClass() == fragment.getClass()) {
				return true;
			}
		}
		return false;
	}

}