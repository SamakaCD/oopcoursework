package ua.iyps.oopcoursework.presentation.additem;

import android.widget.EditText;

import butterknife.BindView;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.utils.ValidatorHelper;

/**
 * Фрагмент, який відповідає за запис тривалості
 */
@Layout(R.layout.fragment_add_duration)
public class DurationFragment extends BaseFragment implements ValidatorHelper.Validatable {

	@BindView(R.id.durationField)
	EditText editText;

	AddItemContract contract;
	AudioFile audioFile;

	public DurationFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ValidatorHelper.bind(editText, this);
		if(audioFile.duration != null) {
			editText.setText(String.valueOf(audioFile.duration));
		}
	}

	@Override
	public boolean validate() {
		String input = editText.getText().toString();
		try {
			if(input.length() > 0 & input.length() > 8)
				return false;

			Integer.parseInt(input);
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void onValid() {
		audioFile.duration = Integer.parseInt(editText.getText().toString());
		contract.addFragment(new CodecFragment(contract));
		contract.setPageIndex(3);
	}

	@Override
	public void onError() {
		showError("Некоректно введена тривалість файлу, перевірте написання та повторіть спробу.");
	}

	@Override
	public void onSilentLock() {
		contract.setPagingEnabled(false);
	}

	@Override
	public void onSilentUnlock() {
		contract.setPagingEnabled(true);
	}

}