package ua.iyps.oopcoursework.presentation.additem;

import teamvoy.fragmentpico.Fragment;
import ua.iyps.oopcoursework.entity.AudioFile;

/**
 * Контракт для взаємодії дочірніх сторінок-фрагментів та батьківського
 */
public interface AddItemContract {

	/**
	 * Дозволяє або забороняє скроллінг сторінок
	 * @param pagingEnabled визначає, чи дозволити скролінг
	 */
	void setPagingEnabled(boolean pagingEnabled);

	/**
	 * Встановлює поточну сторінку
	 * @param index індекс сторінки
	 */
	void setPageIndex(int index);

	/**
	 * Додає нову сторінку-фрагмент
	 * @param fragment фрагмент для додавання
	 */
	void addFragment(Fragment fragment);

	/**
	 * Викликається, коли останній пункт був успішно доданий та провалідований
	 */
	void onFinishAdding();

	/**
	 * Повертає аудіофайл, який редагується
	 * @return
	 */
	AudioFile getAudioFile();

}