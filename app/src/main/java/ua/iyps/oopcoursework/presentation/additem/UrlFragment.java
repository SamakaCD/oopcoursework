package ua.iyps.oopcoursework.presentation.additem;

import android.text.Editable;
import android.text.Selection;
import android.widget.EditText;

import org.apache.commons.validator.routines.UrlValidator;

import butterknife.BindView;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.presentation.custom.TextWatcherAdapter;
import ua.iyps.oopcoursework.utils.ValidatorHelper;

/**
 * Фрагмент, який відповідає за запис адреси файлу в мережі
 */
@Layout(R.layout.fragment_add_url)
public class UrlFragment extends BaseFragment implements ValidatorHelper.Validatable {

	private static final String HTTP_PREFIX = "http://";

	@BindView(R.id.urlField)
	EditText editText;

	AddItemContract contract;
	AudioFile audioFile;

	public UrlFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ValidatorHelper.bind(editText, this);
		editText.setText(HTTP_PREFIX);
		Selection.setSelection(editText.getText(), editText.getText().length());
		editText.addTextChangedListener(new TextWatcherAdapter() {
			@Override
			public void afterTextChanged(Editable s) {
				if (!s.toString().contains(HTTP_PREFIX)) {
					editText.setText(HTTP_PREFIX);
					Selection.setSelection(editText.getText(), editText.getText().length());
				}
			}
		});

		if (audioFile.url != null) {
			if (audioFile.url.startsWith(HTTP_PREFIX)) {
				editText.append(audioFile.url.substring(HTTP_PREFIX.length()));
			} else {
				editText.append(audioFile.url);
			}
		}
	}

	@Override
	public boolean validate() {
		String input = editText.getText().toString();
		return UrlValidator.getInstance().isValid(input) & input.length() < 255;
	}

	@Override
	public void onValid() {
		audioFile.url = editText.getText().toString();
		contract.addFragment(new ExtFragment(contract));
		contract.setPageIndex(1);
	}

	@Override
	public void onError() {
		if (editText.getText().toString().length() >= 255) {
			showError("Занадто довга URL-адреса файлу. Спробуйте ввести кортшу адресу.");
		} else {
			showError("Некоректно введена URL-адреса файлу, перевірте написання та повторіть спробу.");
		}
	}

	@Override
	public void onSilentLock() {
		contract.setPagingEnabled(false);
	}

	@Override
	public void onSilentUnlock() {
		contract.setPagingEnabled(true);
	}

}