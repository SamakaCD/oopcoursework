package ua.iyps.oopcoursework.presentation.custom.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.os.Handler;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import teamvoy.fragmentpico.Fragment;
import teamvoy.fragmentpico.FragmentAttacher;

public class AnimatedFragmentViewAttacher extends FragmentAttacher {

	private static final int BACKGROUND_VIEW_REMOVE_DELAY = 150;

	protected boolean useBackgroundView, useAnimation;

	private ViewGroup mParent;
	private int mDefaultOffset;
	private int mBackgroundColor;

	private int mAttachAnimationDuration, mDetachAnimationDuration;
	private TimeInterpolator mAttachInterpolator, mDetachInterpolator;
	private boolean isRunning;
	private Handler mHandler;

	public AnimatedFragmentViewAttacher(ViewGroup parent, int backgroundColor) {
		mParent = parent;
		mBackgroundColor = backgroundColor;
		mDefaultOffset = (int) (parent.getResources().getDisplayMetrics().density * 64);

		mAttachAnimationDuration = 300;
		mDetachAnimationDuration = 150;
		mAttachInterpolator = new FastOutSlowInInterpolator();
		mDetachInterpolator = new FastOutLinearInInterpolator();
		mHandler = new Handler();
	}

	@Override
	public void attach(Fragment fragment) {
		final View backgroundView = prepareBackgroundView();
		fragment.updateContext(mParent.getContext());
		fragment.setupView();

		mParent.addView(backgroundView);
		mParent.addView(fragment.getView());
		animateBackgroundViewAttach(backgroundView);
		animateFragmentViewAttach(fragment.getView());

		isRunning = true;
		mHandler.postDelayed(() -> isRunning = false, 300);
	}

	@Override
	public void replace(Fragment attachingFragment, List<Fragment> fragmentsForDetach) {
		attach(attachingFragment);
		mParent.postDelayed(() -> {
			for(Fragment fragment : fragmentsForDetach) {
				forceDetach(fragment);
			}
		}, mAttachAnimationDuration);
	}

	@Override
	public void show(Fragment fragment) {
		fragment.getView().setVisibility(View.VISIBLE);
	}

	@Override
	public void hide(final Fragment fragment) {
		fragment.getView().postDelayed(() ->
				fragment.getView().setVisibility(View.INVISIBLE), mAttachAnimationDuration);
	}

	@Override
	public void detach(final Fragment fragment) {
		View backgroundView = prepareBackgroundView();
		mParent.addView(backgroundView, 1);
		animateFragmentViewDetach(fragment.getView());
		animateBackgroundViewDetach(backgroundView);

		isRunning = true;
		mHandler.postDelayed(() -> isRunning = false, 150);
	}

	@Override
	public void forceDetach(Fragment fragment) {
		mParent.removeView(fragment.getView());
	}

	@Override
	public void detachAll() {
		mParent.removeAllViews();
	}

	@Override
	public boolean isTransitionInProgress() {
		return isRunning;
	}

	private View prepareBackgroundView() {
		View backgroundView = new View(mParent.getContext());
		backgroundView.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		backgroundView.setBackgroundColor(mBackgroundColor);
		return backgroundView;
	}

	private void animateFragmentViewAttach(View fragmentView) {
		fragmentView.setTranslationY(mDefaultOffset);
		fragmentView.setAlpha(0);
		fragmentView.animate()
				.setDuration(mAttachAnimationDuration)
				.setInterpolator(mAttachInterpolator)
				.translationY(0)
				.alpha(1)
				.start();
	}

	private void animateFragmentViewDetach(View fragmentView) {
		fragmentView.animate()
				.setDuration(mDetachAnimationDuration)
				.setInterpolator(mDetachInterpolator)
				.translationY(mDefaultOffset)
				.alpha(0)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mParent.removeView(fragmentView);
					}
				}).start();
	}

	private void animateBackgroundViewAttach(View backgroundView) {
		backgroundView.setClickable(true);
		backgroundView.setTranslationY(mDefaultOffset);
		backgroundView.setAlpha(0);
		backgroundView.animate()
				.setDuration(mAttachAnimationDuration)
				.setInterpolator(mAttachInterpolator)
				.translationY(0)
				.alpha(1)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mParent.postDelayed(() -> mParent.removeView(backgroundView),
								BACKGROUND_VIEW_REMOVE_DELAY);
					}
				}).start();
	}

	private void animateBackgroundViewDetach(View backgroundView) {
		backgroundView.animate()
				.setDuration(mDetachAnimationDuration)
				.setInterpolator(mDetachInterpolator)
				.translationY(mDefaultOffset)
				.alpha(0)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mParent.postDelayed(() -> mParent.removeView(backgroundView),
								BACKGROUND_VIEW_REMOVE_DELAY);
					}
				})
				.start();
	}

}