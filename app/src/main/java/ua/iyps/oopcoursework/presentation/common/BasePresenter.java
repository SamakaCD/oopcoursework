package ua.iyps.oopcoursework.presentation.common;

import java.util.Map;

public class BasePresenter<VIEW> {

	private VIEW mView;
	private Map<String, Object> arguments;

	protected void onStart() {

	}

	protected void onStop() {

	}

	public void setView(VIEW view) {
		mView = view;
	}

	public VIEW getView() {
		return mView;
	}

	public void setArguments(Map<String, Object> arguments) {
		this.arguments = arguments;
	}

	public int getIntArgument(String key) {
		return (int) arguments.get(key);
	}

	public String getStringArgument(String key) {
		return (String) arguments.get(key);
	}

	public Object getArgument(String key) {
		return arguments.get(key);
	}

}