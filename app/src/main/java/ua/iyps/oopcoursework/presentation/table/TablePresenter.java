package ua.iyps.oopcoursework.presentation.table;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import teamvoy.fragmentpico.FragmentHolder;
import ua.iyps.oopcoursework.Constants;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.additem.AddItemFragment;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;
import ua.iyps.oopcoursework.presentation.openfile.OpenFileFragment;
import ua.iyps.oopcoursework.presentation.savefile.SaveFileFragment;

/**
 * Presenter фрагмента таблиці
 */
public class TablePresenter extends BasePresenter<TableView> {

	/**
	 * Контейнер фрагментів
	 */
	@Inject
	FragmentHolder fragmentHolder;

	/**
	 * Оригінальний список аудіофайлів
	 */
	List<AudioFile> originalAudioFiles;

	/**
	 * Список, який відображається в даний час (наприклад, відфільтрований)
	 */
	List<AudioFile> currentAudioFiles;

	/**
	 * Позиція виділеного в даний момент рядка таблиці
	 */
	int selectedAudioFilePosition;

	/**
	 * Відкритий в даний момент файл
	 */
	File openedFile;

	/**
	 * Визначає, чи зараз відкритий якийсь файл
	 */
	boolean wasFileOpened;

	/**
	 * Визначає, чи у файлі були зміни
	 */
	boolean hasChanges;

	/**
	 * Конструктор ін'єкції
	 */
	@Inject
	public TablePresenter() { }

	/**
	 * Викликається на старті Presenter
	 */
	@Override
	protected void onStart() {
		super.onStart();
		originalAudioFiles = new ArrayList<>();
		currentAudioFiles = new ArrayList<>();

		getView().bindList(currentAudioFiles);
		openedFile = (File) getArgument(Constants.ARG_FILE); // Якщо в аргументах є якийсь файл, відкриємо його
		if (openedFile != null) {
			readFile(openedFile);
		}
	}

	/**
	 * Викликається при потребі створити новий аудіофайл
	 */
	public void onAddItem() {
		fragmentHolder.add(new AddItemFragment()
				.putArgument(Constants.ARG_ADD_ITEM_CALLBACK, onAudioFileCreated));
	}

	/**
	 * Викликається при кліку на пункт списку
	 * @param position позиція елементу
	 */
	public void onItemClick(int position) {
		selectedAudioFilePosition = position;
		getView().showItemDialog();
	}

	/**
	 * Викликається при потребі відредагувати виділений пункт списку
	 */
	public void onEditItem() {
		fragmentHolder.add(new AddItemFragment()
				.putArgument(Constants.ARG_EDIT_ITEM, currentAudioFiles.get(selectedAudioFilePosition))
				.putArgument(Constants.ARG_EDIT_FINISHED_CALLBACK, onAudioFileEdited));
	}

	/**
	 * Викликається при потребі видалити виділений пункт списку
	 */
	public void onDeleteItem() {
		getView().showConfirmDeleteDialog();
	}

	/**
	 * Підтверджує видалення аудіофайлу
	 */
	public void confirmDelete() {
		AudioFile forRemove = currentAudioFiles.remove(selectedAudioFilePosition);
		originalAudioFiles.remove(forRemove);
		getView().updateList();
		hasChanges = true;
	}

	/**
	 * Викликається при потребі зберегти файл
	 */
	public void saveFile() {
		if (!wasFileOpened) {
			fragmentHolder.add(new SaveFileFragment()
					.putArgument(Constants.ARG_SAVE_FILE_CALLBACK, onSaveDialogConfirmed));
		} else {
			onSaveDialogConfirmed.call(openedFile);
		}
	}

	/**
	 * Викликається після кліку на кнопку "Зберегти як" (довгий клік на "Зберегти")
	 */
	public void saveAs() {
		fragmentHolder.add(new SaveFileFragment()
				.putArgument(Constants.ARG_SAVE_FILE_CALLBACK, onSaveDialogConfirmed));
	}

	/**
	 * Викликається при потребі зберегти і створити новий файл
	 */
	public void saveAndCreateFile() {
		if (!wasFileOpened) {
			fragmentHolder.add(new SaveFileFragment()
					.putArgument(Constants.ARG_SAVE_FILE_CALLBACK, onSaveAndPreCreateFile));
		} else {
			onSaveAndPreCreateFile.call(openedFile);
		}
	}

	/**
	 * Створює новий файл і відкриває новий відповідний фрагмент
	 */
	public void createNewFileAndFragment() {
		fragmentHolder.clear();
		fragmentHolder.add(new TableFragment());
	}

	/**
	 * Викликається при потребі створити новий файл.
	 * Якщо були зміни, покажеться діалог з пропозицією зберегти зміни
	 */
	public void onRequestCreateNewFile() {
		if (hasChanges) {
			getView().showSaveSuggestionDialog(this::saveAndCreateFile, this::createNewFileAndFragment);
		} else {
			createNewFileAndFragment();
		}
	}

	/**
	 * Виклкається при потребі відкрити файл.
	 * Якщо були зміни, покажеться діалог з пропозицією зберегти зміни
	 */
	public void onRequestOpenFile() {
		if (hasChanges) {
			getView().showSaveSuggestionDialog(this::saveAndOpenFile, this::openFile);
		} else {
			openFile();
		}
	}

	/**
	 * Зберігає файл і відкриває фрагмент відкриття файлу
	 */
	public void saveAndOpenFile() {
		if (!wasFileOpened) {
			fragmentHolder.add(new SaveFileFragment()
					.putArgument(Constants.ARG_SAVE_FILE_CALLBACK, onSaveAndOpenFile));
		} else {
			onSaveAndOpenFile.call(openedFile);
		}
	}

	/**
	 * Відкриває фрагмент відкриття файлу
	 */
	public void openFile() {
		fragmentHolder.add(new OpenFileFragment()
				.putArgument(Constants.ARG_OPEN_FILE_CALLBACK, onFileOpened));
	}

	/**
	 * Викликається при зміні вмісту полів пошуку
	 */
	public void onSearchFieldTextChanged() {
		Stream<AudioFile> audioFileStream = Stream.of(originalAudioFiles);
		Integer bitrate = getView().getBitrateForSearch();
		String codec = getView().getCodecForSearch();
		if(bitrate != null) {
			String stringBitrate = String.valueOf(((int) bitrate));
			audioFileStream = audioFileStream.filter(value ->
					String.valueOf(((int) value.bitrate)).contains(stringBitrate));
		}
		if(codec != null) {
			audioFileStream = audioFileStream.filter(value ->
					value.codec.toLowerCase().contains(codec.toLowerCase()));
		}
		currentAudioFiles.clear();
		currentAudioFiles.addAll(groupAndSort(audioFileStream.collect(Collectors.toList())));
		getView().updateList();
	}

	/**
	 * Зчитує файл зі списком аудіофайлів
	 * @param file файл, який необхідно прочитати
	 */
	private void readFile(File file) {
		originalAudioFiles.clear();
		getView().updateList();
		getView().showWaitDialog();
		getReadFileObservable(file)
				.toList()
				.map(this::groupAndSort)
				.subscribe(audioFiles -> { // список аудіофайлів
					for(AudioFile audioFile : audioFiles) {
						originalAudioFiles.add(audioFile);
						currentAudioFiles.add(audioFile);
					}
				}, error -> { // помилка
					error.printStackTrace();
					getView().hideWaitDialog();
					getView().showErrorDialog(error.getMessage());
				}, () -> { // завершення зчитування
					wasFileOpened = true;
					getView().hideWaitDialog();
					getView().updateList();
				});
	}

	/**
	 * Повертає асинхронний Observable зі списком аудіофайлів, зчитаних з файлу
	 * @param file файл, який необхідно прочитати
	 * @return Observable зі списком аудіофайлів
	 */
	private Observable<AudioFile> getReadFileObservable(File file) {
		return Observable.fromCallable(() -> AudioFile.readFromFile(file))
				.flatMap(Observable::from)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread());
	}

	/**
	 * Повертає асинхронний Observable, який збереже список аудіофайлів у файл
	 * @param file файл, у який будуть збережені аудіофайли
	 * @param audioFiles список аудіофайлів для зберігання
	 * @return Observable, у якому викличеться onNext при успішному зберіганні
	 */
	private Observable<Void> getWriteFileObservable(File file, List<AudioFile> audioFiles) {
		return Observable.fromCallable(() -> {
			AudioFile.saveToFile(file, audioFiles);
			return null;
		}).subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.map(o -> null);
	}

	/**
	 * Групує список по URL та сортує кожену групо по зростанню частоти дискретизації
	 * @param files вхідний список аудіофайлів
	 * @return погрупований та посортований список аудіофайлів
	 */
	private List<AudioFile> groupAndSort(List<AudioFile> files) {
		Map<String, List<AudioFile>> groupMap = new TreeMap<>();
		for (AudioFile file : files) {
			List<AudioFile> list = groupMap.get(file.url);
			if (list == null) {
				list = new ArrayList<>();
				groupMap.put(file.url, list);
			}
			list.add(file);
		}

		for (List<AudioFile> audioFiles : groupMap.values()) {
			Collections.sort(audioFiles, AudioFile::compareBySampleRate);
		}

		List<AudioFile> result = new ArrayList<>();
		for (List<AudioFile> audioFiles : groupMap.values()) {
			result.addAll(audioFiles);
		}

		return result;
	}

	/**
	 * Викликається, коли був доданий новий аудіофайл
	 */
	private Action1<AudioFile> onAudioFileCreated = audioFile -> {
		originalAudioFiles.add(audioFile);
		List<AudioFile> groupedList = groupAndSort(originalAudioFiles);
		originalAudioFiles.clear();
		originalAudioFiles.addAll(groupedList);
		currentAudioFiles.clear();
		currentAudioFiles.addAll(groupedList);
		getView().updateList();
		hasChanges = true;
	};

	/**
	 * Викликається, коли аудіофайл був відредагований
	 */
	private Action0 onAudioFileEdited = () -> {
		List<AudioFile> groupedList = groupAndSort(originalAudioFiles);
		originalAudioFiles.clear();
		originalAudioFiles.addAll(groupedList);
		currentAudioFiles.clear();
		currentAudioFiles.addAll(groupedList);
		getView().updateList();
		hasChanges = true;
	};

	/**
	 * Викликається, коли потрібно зберегти файл
	 */
	private Action1<File> onSaveDialogConfirmed = file -> {
		getView().showWaitDialog();
		getWriteFileObservable(file, originalAudioFiles)
				.subscribe(
						success -> {
							openedFile = file;
							hasChanges = false;
							wasFileOpened = true;
							getView().hideWaitDialog();
						}, error -> {
							error.printStackTrace();
							getView().hideWaitDialog();
							getView().showError("Виникла помилка при збергінні файлу");
						}
				);
	};

	/**
	 * Викикається, коли потрібно зберегти і створити новий файл
	 */
	private Action1<File> onSaveAndPreCreateFile = file -> {
		getView().showWaitDialog();
		getWriteFileObservable(file, originalAudioFiles)
				.subscribe(
						success -> {
							openedFile = file;
							hasChanges = false;
							wasFileOpened = true;
							getView().hideWaitDialog();
							createNewFileAndFragment();
						}, error -> {
							error.printStackTrace();
							getView().hideWaitDialog();
							getView().showError("Виникла помилка при збергінні файлу");
						}
				);
	};

	/**
	 * Викикається, коли потрібно зберегти і відкрити файл
	 */
	private Action1<File> onSaveAndOpenFile = file -> {
		getView().showWaitDialog();
		getWriteFileObservable(file, originalAudioFiles)
				.doOnNext(o -> openFile())
				.subscribe(
						success -> {
							openedFile = file;
							hasChanges = false;
							wasFileOpened = true;
							getView().hideWaitDialog();
							openFile();
						}, error -> {
							error.printStackTrace();
							getView().hideWaitDialog();
							getView().showError("Виникла помилка при збергінні файлу");
						}
				);
	};

	/**
	 * Викликається, коли був відкритий інший файл
	 */
	private Action1<File> onFileOpened = file -> {
		fragmentHolder.clear();
		fragmentHolder.add(new TableFragment()
				.putArgument(Constants.ARG_FILE, file));
	};

}