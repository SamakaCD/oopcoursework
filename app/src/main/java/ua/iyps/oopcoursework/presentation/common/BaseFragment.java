package ua.iyps.oopcoursework.presentation.common;

import android.animation.TimeInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import butterknife.ButterKnife;
import teamvoy.fragmentpico.Fragment;
import ua.iyps.oopcoursework.presentation.custom.Snackbar;

public class BaseFragment extends Fragment implements BaseView {

	protected static final TimeInterpolator SHOW_INTERPOLATOR = new DecelerateInterpolator(7);

	@Override
	public View onCreateView() {
		int layoutId = getLayoutId();
		if(layoutId != 0) {
			return LayoutInflater.from(getContext()).inflate(layoutId, null);
		} else {
			return null;
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		inject();
		ButterKnife.bind(this, getView());
		if(getPresenter() != null) {
			getPresenter().setView(this);
			getPresenter().setArguments(getArguments());
		}
	}

	@Override
	public void onPostCreate() {
		super.onPostCreate();
		if(getPresenter() != null) {
			getPresenter().onStart();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(getPresenter() != null) {
			getPresenter().onStop();
		}
	}

	@Override
	public void showError(String message) {
		Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
	}

	@Override
	public void showError(int messageStringResId) {
		showError(getString(messageStringResId));
	}

	private int getLayoutId() {
		Layout layoutAnnotation = getClass().getAnnotation(Layout.class);
		if(layoutAnnotation != null) {
			return layoutAnnotation.value();
		} else {
			return 0;
		}
	}

	protected void inject() {

	}

	protected BasePresenter getPresenter() {
		return null;
	}

	protected int dp(int dp) {
		return (int) (getView().getResources().getDisplayMetrics().density * dp);
	}

	protected void delayed(int delay, Runnable runnable) {
		getView().postDelayed(runnable, delay);
	}

	protected String getString(int stringRes) {
		return getContext().getString(stringRes);
	}

}