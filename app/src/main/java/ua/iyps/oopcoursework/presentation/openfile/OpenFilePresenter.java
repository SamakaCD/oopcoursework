package ua.iyps.oopcoursework.presentation.openfile;

import com.annimon.stream.Stream;

import java.io.File;

import javax.inject.Inject;

import rx.functions.Action1;
import ua.iyps.oopcoursework.Constants;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;

/**
 * Presenter фрагмента відкриття файлу
 */
public class OpenFilePresenter extends BasePresenter<OpenFileView> {

	/**
	 * Поточна папка
	 */
	private File currentFolder;

	/**
	 * Конструктор ін'єкції
	 */
	@Inject
	public OpenFilePresenter() { }

	/**
	 * Відбувається після старту фрагмента
	 */
	@Override
	protected void onStart() {
		super.onStart();
		goTo(new File("/sdcard/"));
	}

	/**
	 * Викликається, коли користувач вибрав пункт списку
	 * @param type тип пункту
	 * @param file об'єкт типу File, якщо пункт є файлом або папкою, інакше null
	 */
	public void onListItemSelected(OpenFileListAdapter.Item.Type type, File file) {
		switch (type) {
			case FOLDER:
				goTo(file);
				break;
			case FILE:
				getView().fileSelected();
				((Action1) getArgument(Constants.ARG_OPEN_FILE_CALLBACK)).call(file);
				break;
			case UP:
				goTo(currentFolder.getParentFile());
				break;
		}
	}

	/**
	 * Викликається при натисканні користувачем кнопки "назад"
	 * @return true, якщо фрагмент можна закрити, інакше false
	 */
	public boolean canExit() {
		if(isRootFolder(currentFolder)) {
			return true;
		} else {
			goTo(currentFolder.getParentFile());
			return false;
		}
	}

	/**
	 * Переходить у папку
	 * @param folder папка, у яку необхідно перейти
	 */
	private void goTo(File folder) {
		getView().clearList();
		getView().setCurrentFolder(folder.getAbsolutePath());
		currentFolder = folder;

		// Якщо папка не корінна, додаємо пункт "угору"
		if(!isRootFolder(folder)) {
			getView().addUp();
		}

		// Додавання папок
		Stream.of(folder.listFiles())
				.filter(File::isDirectory)
				.filter(file -> !file.getName().startsWith("."))
				.sortBy(File::getName)
				.forEach(file -> getView().addFolder(file));

		// Додавання файлів
		Stream.of(folder.listFiles())
				.filter(File::isFile)
				.filter(file -> !file.getName().startsWith("."))
				.sortBy(File::getName)
				.forEach(file -> getView().addFile(file));
	}

	/**
	 * Перевіряє чи папка є корінною
	 * @param file папка для перевірки
	 * @return true, якщо папка корінна, інакше false
	 */
	private boolean isRootFolder(File file) {
		return file.getName().contains("sdcard");
	}

}