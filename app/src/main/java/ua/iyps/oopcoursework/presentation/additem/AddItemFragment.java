package ua.iyps.oopcoursework.presentation.additem;

import android.view.View;

import com.badoualy.stepperindicator.StepperIndicator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.functions.Action0;
import rx.functions.Action1;
import teamvoy.fragmentpico.Fragment;
import teamvoy.fragmentpico.FragmentHolder;
import ua.iyps.oopcoursework.Constants;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.di.Injector;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.presentation.custom.CustomViewPager;
import ua.iyps.oopcoursework.presentation.custom.fragment.FragmentPagerAdapter;

/**
 * Фрагмент, який відповідає за додавання нового аудіофайлу
 */
@Layout(R.layout.fragment_add_item)
public class AddItemFragment extends BaseFragment implements AddItemContract {

	/**
	 * Контейнер фрагментів
	 */
	@Inject
	FragmentHolder fragmentHolder;

	/**
	 *  Контейнер сторінок
	 */
	@BindView(R.id.pager)
	CustomViewPager viewPager;

	/**
	 * Індикатор поточної сторінки
	 */
	@BindView(R.id.stepper)
	StepperIndicator stepperIndicator;

	/**
	 * Кнопка закриття
	 */
	@BindView(R.id.closeButton)
	View closeButton;

	/**
	 * Адаптер контейнера сторінок
	 */
	FragmentPagerAdapter pagerAdapter;

	/**
	 * Аудіофайл, який зараз редагується
	 */
	AudioFile audioFile;

	/**
	 * Визначає, чи зараз відбувається редагування вже створеного аудіофайлу
	 */
	boolean isEditing;

	/**
	 * Викликається при створенні фрагмента
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		audioFile = (AudioFile) getArgument(Constants.ARG_EDIT_ITEM);
		if(audioFile == null) {
			audioFile = new AudioFile();
		} else {
			isEditing = true;
		}

		setupViewPager();
		closeButton.setFocusable(false);
		closeButton.setFocusableInTouchMode(false);

		// Якщо ми редагуємо, додамо всі фрагменти
		if(isEditing) {
			pagerAdapter.addFragment(new ExtFragment(this));
			pagerAdapter.addFragment(new DurationFragment(this));
			pagerAdapter.addFragment(new CodecFragment(this));
			pagerAdapter.addFragment(new HasKaraokeTextFragment(this));
			pagerAdapter.addFragment(new SampleRateFragment(this));
			pagerAdapter.addFragment(new BitrateFragment(this));
			pagerAdapter.addFragment(new PlayerFragment(this));
		}
	}

	/**
	 * Викликається при кліку на кнопку закриття
	 */
	@OnClick(R.id.closeButton)
	void onClose() {
		fragmentHolder.back();
	}

	/**
	 * Викликається при необхідності зробити ін'єкцію залежностей
	 */
	@Override
	protected void inject() {
		Injector.getAppComponent().inject(this);
	}

	@Override
	public void onFinishAdding() {
		fragmentHolder.back();
		if(isEditing) {
			((Action0) getArgument(Constants.ARG_EDIT_FINISHED_CALLBACK)).call();
		} else {
			((Action1<AudioFile>) getArgument(Constants.ARG_ADD_ITEM_CALLBACK)).call(audioFile);
		}
	}

	@Override
	public void setPagingEnabled(boolean pagingEnabled) {
		viewPager.setPagingEnabled(pagingEnabled);
	}

	@Override
	public void setPageIndex(int index) {
		viewPager.setCurrentItem(index);
		stepperIndicator.setCurrentStep(index);
	}

	@Override
	public void addFragment(Fragment fragment) {
		pagerAdapter.addFragment(fragment);
	}

	@Override
	public AudioFile getAudioFile() {
		return audioFile;
	}

	/**
	 * Налагоджує контейнер сторінок
	 */
	private void setupViewPager() {
		pagerAdapter = new FragmentPagerAdapter(getContext());
		pagerAdapter.addFragment(new UrlFragment(this));
		viewPager.setAdapter(pagerAdapter);
		stepperIndicator.setViewPager(viewPager);
		stepperIndicator.setStepCount(7);
	}

}