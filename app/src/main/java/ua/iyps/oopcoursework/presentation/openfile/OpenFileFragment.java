package ua.iyps.oopcoursework.presentation.openfile;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import teamvoy.fragmentpico.FragmentHolder;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.di.Injector;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;
import ua.iyps.oopcoursework.presentation.common.Layout;

/**
 * Фрагмент відкриття файлу
 */
@Layout(R.layout.fragment_open_file)
public class OpenFileFragment extends BaseFragment implements OpenFileView {

	/**
	 * Presenter фрагмента
	 */
	@Inject
	OpenFilePresenter presenter;

	/**
	 * Контейнер фрагмента
	 */
	@Inject
	FragmentHolder fragmentHolder;

	/**
	 * Відображення поточного шляху
	 */
	@BindView(R.id.currentFolder)
	TextView currentFolder;

	/**
	 * Список
	 */
	@BindView(R.id.recyclerView)
	RecyclerView recyclerView;

	/**
	 * Адаптер списку
	 */
	OpenFileListAdapter adapter;

	/**
	 * Викликається при створенні фрагменту
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		setupRecyclerView();
	}

	/**
	 * Викликається при запиті на відкріплення фрагмента
	 * @return true, якщо фрагмент можна відкріпити, інакше false
	 */
	@Override
	public boolean onRequestDetach() {
		return presenter.canExit();
	}

	/**
	 * Встановлює шлях до поточної папки
	 * @param path шлях до поточної папки
	 */
	@Override
	public void setCurrentFolder(String path) {
		currentFolder.setText(path);
	}

	/**
	 * Очищає список
	 */
	@Override
	public void clearList() {
		adapter.clear();
	}

	/**
	 * Додає пункт "угору"
	 */
	@Override
	public void addUp() {
		adapter.addUp();
	}

	/**
	 * Додає пункт "папка"
	 * @param folder папка, якій відповідатиме пункт
	 */
	@Override
	public void addFolder(File folder) {
		adapter.addFolder(folder);
	}

	/**
	 * Додає пункт "файл"
	 * @param file файл, якому відповідатиме пункт
	 */
	@Override
	public void addFile(File file) {
		adapter.addFile(file);
	}

	/**
	 * Сповіщає фрагмент про те, що файл вибраний
	 */
	@Override
	public void fileSelected() {
		fragmentHolder.remove(this);
	}

	/**
	 * Викликається при необхідності зробити ін'єкцію залежностей
	 */
	@Override
	protected void inject() {
		Injector.getAppComponent().inject(this);
	}

	/**
	 * Повертає Presenter цього фрагмента
	 */
	@Override
	protected BasePresenter getPresenter() {
		return presenter;
	}

	/**
	 * Установлює параметри та адаптер списку
	 */
	private void setupRecyclerView() {
		LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
		adapter = new OpenFileListAdapter();
		adapter.setOnItemSelectedListener(presenter::onListItemSelected);
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setAdapter(adapter);
	}

}