package ua.iyps.oopcoursework.presentation.additem;

import android.view.View;
import android.widget.RadioButton;

import butterknife.BindView;
import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;

/**
 * Фрагмент, який відповідає за запис інформації про наявність тексту караоке
 */
@Layout(R.layout.fragment_add_has_karaoke)
public class HasKaraokeTextFragment extends BaseFragment {

	@BindView(R.id.hasKaraokeYes)
	RadioButton hasKaraokeYesButton;

	@BindView(R.id.hasKaraokeNo)
	RadioButton hasKaraokeNoButton;

	@BindView(R.id.hasKaraokeFab)
	View fab;

	AddItemContract contract;
	AudioFile audioFile;

	public HasKaraokeTextFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		if(audioFile.hasKaraokeText) {
			hasKaraokeYesButton.setChecked(true);
		} else {
			hasKaraokeNoButton.setChecked(true);
		}
	}

	@OnClick(R.id.hasKaraokeYes)
	void onHasKaraokeYesClick() {
		audioFile.hasKaraokeText = true;
		fab.requestFocus();
	}

	@OnClick(R.id.hasKaraokeNo)
	void onHasKaraokeNo() {
		audioFile.hasKaraokeText = false;
		fab.requestFocus();
	}

	@OnClick(R.id.hasKaraokeFab)
	void onFabClick() {
		contract.addFragment(new SampleRateFragment(contract));
		contract.setPageIndex(5);
	}

}