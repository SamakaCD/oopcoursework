package ua.iyps.oopcoursework.presentation.table;

import android.app.ProgressDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import teamvoy.fragmentpico.FragmentHolder;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.di.Injector;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.AboutFragment;
import ua.iyps.oopcoursework.presentation.HelpFragment;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.presentation.custom.ErrorDialog;
import ua.iyps.oopcoursework.presentation.custom.TextWatcherAdapter;
import ua.iyps.oopcoursework.presentation.empty.EmptyFragment;

/**
 * Фрагмент з таблицею
 */
@Layout(R.layout.fragment_table)
public class TableFragment extends BaseFragment implements TableView {

	/**
	 * Presenter фрагмента
	 */
	@Inject
	TablePresenter presenter;

	/**
	 * Контейнер фрагментів
	 */
	@Inject
	FragmentHolder fragmentHolder;

	/**
	 * Список пунктів
	 */
	@BindView(R.id.recyclerView)
	RecyclerView recyclerView;

	/**
	 * Контейнер, який містить поля для пошуку
	 */
	@BindView(R.id.searchToolbar)
	View searchToolbar;

	/**
	 * Поле, яке містить бітрейт для пошуку
	 */
	@BindView(R.id.bitrateSearchField)
	EditText bitrateEditText;

	/**
	 * Поле, яке містить кодек для пошуку
	 */
	@BindView(R.id.codecSearchField)
	EditText codecEditText;

	/**
	 * Адаптер списку (таблиці)
	 */
	TableAdapter adapter;

	/**
	 * Діалог про виконання фонової операції
	 */
	ProgressDialog waitDialog;

	/**
	 * Викликається при створенні фрагмента
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		setupRecyclerView();
		setupSearchFields();
	}

	@Override
	public void bindList(List<AudioFile> items) {
		adapter.bind(items);
	}

	@Override
	public void updateList() {
		adapter.notifyDataSetChanged();
	}

	@Override
	public void showWaitDialog() {
		waitDialog = new ProgressDialog(getContext());
		waitDialog.setCancelable(false);
		waitDialog.setMessage("Будь ласка, зачекайте");
		waitDialog.setIndeterminate(true);
		waitDialog.show();
	}

	@Override
	public void hideWaitDialog() {
		if (waitDialog != null) {
			waitDialog.dismiss();
		}
	}

	@Override
	public void showErrorDialog(String message) {
		ErrorDialog.show(getContext(), message, () -> {
			fragmentHolder.clear();
			fragmentHolder.add(new EmptyFragment());
		});
	}

	@Override
	public void showItemDialog() {
		AlertDialog alertDialog = new AlertDialog.Builder(getContext())
				.setView(R.layout.dialog_table_item)
				.show();
		alertDialog.findViewById(R.id.edit_item).setOnClickListener(v -> {
			alertDialog.dismiss();
			presenter.onEditItem();
		});
		alertDialog.findViewById(R.id.delete_item).setOnClickListener(v -> {
			alertDialog.dismiss();
			presenter.onDeleteItem();
		});
	}

	@Override
	public void showConfirmDeleteDialog() {
		new AlertDialog.Builder(getContext())
				.setMessage("Ви впевнені, що хочете видалити цей аудіофайл?")
				.setPositiveButton("Ні", (dialog, which) -> dialog.dismiss())
				.setNegativeButton("Так", (dialog, which) -> {
					dialog.dismiss();
					presenter.confirmDelete();
				}).show();
	}

	@Override
	public void showSaveSuggestionDialog(Runnable yes, Runnable no) {
		new AlertDialog.Builder(getContext())
				.setMessage("Зберегти поточні зміни?")
				.setPositiveButton("Так", (dialog, which) -> {
					yes.run();
					dialog.dismiss();
				})
				.setNeutralButton("Ні", (dialog, which) -> {
					no.run();
					dialog.dismiss();
				})
				.setNegativeButton("Скасувати", (dialog, which) -> dialog.dismiss())
				.show();
	}

	@Override
	public Integer getBitrateForSearch() {
		String input = bitrateEditText.getText().toString();
		if(TextUtils.isEmpty(input)) {
			return null;
		} else {
			return Integer.valueOf(input);
		}
	}

	@Override
	public String getCodecForSearch() {
		String input = codecEditText.getText().toString();
		if(TextUtils.isEmpty(input)) {
			return null;
		} else {
			return input;
		}
	}

	@OnClick(R.id.fab)
	void onAddClick() {
		presenter.onAddItem();
	}

	@OnClick(R.id.save)
	void onSaveClick() {
		presenter.saveFile();
	}

	@OnLongClick(R.id.save)
	boolean onSaveAsClick() {
		presenter.saveAs();
		return true;
	}

	@OnClick(R.id.createNewFile)
	void onCreateNewFile() {
		presenter.onRequestCreateNewFile();
	}

	@OnClick(R.id.open)
	void onOpenClick() {
		presenter.onRequestOpenFile();
	}

	@OnClick(R.id.search)
	void onSearchClick() {
		animateSearchToolbarShow();
	}

	@OnClick(R.id.closeSearchButton)
	void onCloseSearchClick() {
		bitrateEditText.setText("");
		codecEditText.setText("");
		animateSearchToolbarHide();
	}

	@OnClick(R.id.help)
	void onHelpClick() {
		fragmentHolder.add(new HelpFragment());
	}

	@OnClick(R.id.about)
	void onAboutClick() {
		fragmentHolder.add(new AboutFragment());
	}

	@Override
	protected void inject() {
		Injector.getAppComponent().inject(this);
	}

	@Override
	protected BasePresenter getPresenter() {
		return presenter;
	}

	/**
	 * Налагоджує список (таблицю)
	 */
	private void setupRecyclerView() {
		adapter = new TableAdapter(presenter);
		recyclerView.setAdapter(adapter);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
	}

	/**
	 * Налагоджує поля пошуку по таблиці
	 */
	private void setupSearchFields() {
		bitrateEditText.addTextChangedListener(new TextWatcherAdapter() {
			@Override
			public void afterTextChanged(Editable s) {
				presenter.onSearchFieldTextChanged();
			}
		});
		codecEditText.addTextChangedListener(new TextWatcherAdapter() {
			@Override
			public void afterTextChanged(Editable s) {
				presenter.onSearchFieldTextChanged();
			}
		});
	}

	/**
	 * Вмикає анімацію показу панелі пошуку
	 */
	private void animateSearchToolbarShow() {
		searchToolbar.animate()
				.setDuration(2000)
				.setInterpolator(new DecelerateInterpolator(7))
				.translationY(0)
				.alpha(1)
				.start();
	}

	/**
	 * Вмикає анімацію приховування панелі пошуку
	 */
	private void animateSearchToolbarHide() {
		searchToolbar.animate()
				.setDuration(300)
				.setInterpolator(new AccelerateInterpolator())
				.translationY(-dp(104))
				.alpha(0)
				.start();
	}

}