package ua.iyps.oopcoursework.presentation.additem;

import android.text.TextUtils;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.utils.ValidatorHelper;

/**
 * Фрагмент, який відповідає за запис назви плеєра
 */
@Layout(R.layout.fragment_add_player)
public class PlayerFragment extends BaseFragment implements ValidatorHelper.Validatable {

	@BindView(R.id.field)
	EditText editText;

	AddItemContract contract;
	AudioFile audioFile;


	public PlayerFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ValidatorHelper.bind(editText, this);
		if(audioFile.playerForOpen != null) {
			editText.setText(audioFile.playerForOpen);
		}
	}

	@Override
	public boolean validate() {
		String input = editText.getText().toString();
		return !TextUtils.isEmpty(input) && input.length() < 255;
	}

	@Override
	public void onValid() {
		audioFile.playerForOpen = editText.getText().toString();
		contract.onFinishAdding();
	}

	@Override
	public void onError() {
		if(editText.getText().length() > 255) {
			showError("Назва плеєра занадто довга.");
		} else {
			showError("Некоректно введене розширення файлу, перевірте написання та повторіть спробу.");
		}
	}

	@Override
	public void onSilentLock() {
		contract.setPagingEnabled(false);
	}

	@Override
	public void onSilentUnlock() {
		contract.setPagingEnabled(true);
	}

	@OnClick(R.id.aimp)
	void onAimpClicked() {
		editText.setText("AIMP");
		editText.requestFocus();
	}

	@OnClick(R.id.winamp)
	void onWinampClicked() {
		editText.setText("Winamp");
		editText.requestFocus();
	}

	@OnClick(R.id.WMP)
	void onWmpClicked() {
		editText.setText("WMP");
		editText.requestFocus();
	}

	@OnClick(R.id.vlc)
	void onVlcClicked() {
		editText.setText("VLC");
		editText.requestFocus();
	}

	@OnClick(R.id.kmp)
	void onKmplicked() {
		editText.setText("KMP");
		editText.requestFocus();
	}

}