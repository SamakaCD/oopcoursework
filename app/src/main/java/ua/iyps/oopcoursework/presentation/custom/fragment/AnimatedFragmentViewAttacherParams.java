package ua.iyps.oopcoursework.presentation.custom.fragment;

import teamvoy.fragmentpico.FragmentAttacher;
import teamvoy.fragmentpico.FragmentAttacherParams;

public class AnimatedFragmentViewAttacherParams extends FragmentAttacherParams {

	public boolean useAnimation;
	public boolean useBackgroundView;

	public AnimatedFragmentViewAttacherParams useBackgroundView() {
		useBackgroundView = true;
		return this;
	}

	public AnimatedFragmentViewAttacherParams useBackgroundView(boolean useBackgroundView) {
		this.useBackgroundView = useBackgroundView;
		return this;
	}

	public AnimatedFragmentViewAttacherParams useAnimation() {
		useAnimation = true;
		return this;
	}

	public AnimatedFragmentViewAttacherParams useAnimation(boolean useAnimation) {
		this.useAnimation = useAnimation;
		return this;
	}

	@Override
	public void onSetupParams(FragmentAttacher fragmentAttacher) {
		AnimatedFragmentViewAttacher attacher = (AnimatedFragmentViewAttacher) fragmentAttacher;
		attacher.useAnimation = useAnimation;
		attacher.useBackgroundView = useBackgroundView;
	}

	@Override
	public void onResetParams(FragmentAttacher fragmentAttacher) {
		AnimatedFragmentViewAttacher attacher = (AnimatedFragmentViewAttacher) fragmentAttacher;
		attacher.useAnimation = true;
		attacher.useBackgroundView = true;
	}

}