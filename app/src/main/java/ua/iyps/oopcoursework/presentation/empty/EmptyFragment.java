package ua.iyps.oopcoursework.presentation.empty;

import javax.inject.Inject;

import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.di.Injector;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;
import ua.iyps.oopcoursework.presentation.common.Layout;

/**
 * Фрагмент порожнього стану
 */
@Layout(R.layout.fragment_empty_state)
public class EmptyFragment extends BaseFragment {

	/**
	 * Presenter фрагмента
	 */
	@Inject
	EmptyPresenter presenter;

	/**
	 * Викликається при кліку на текст "Відкрити файл"
	 */
	@OnClick(R.id.emptyOpenFile)
	void onOpenFile() {
		presenter.onOpenFile();
	}

	/**
	 * Викликається при кліку на текст "Створити файл"
	 */
	@OnClick(R.id.createNewFile)
	void onCreateNewFile() {
		presenter.onCreateNewFile();
	}

	/**
	 * Викликається при необхідності зробити ін'єкцію залежностей
	 */
	@Override
	protected void inject() {
		Injector.getAppComponent().inject(this);
	}

	/**
	 * Повертає Presenter цього фрагмента
	 */
	@Override
	protected BasePresenter getPresenter() {
		return presenter;
	}
}