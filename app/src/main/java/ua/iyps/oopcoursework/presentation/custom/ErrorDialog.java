package ua.iyps.oopcoursework.presentation.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.iyps.oopcoursework.R;

import static ua.iyps.oopcoursework.R.id.showDetails;

public class ErrorDialog {

	@BindView(showDetails)
	View showDetailsButton;

	@BindView(R.id.detailsText)
	TextView detailsTextView;

	public static void show(Context context, String errorMessage, Runnable closeCallback) {
		new ErrorDialog(context, errorMessage, closeCallback);
	}

	private ErrorDialog(Context context, String errorMessage, Runnable closeCallback) {
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		View dialogView = layoutInflater.inflate(R.layout.dialog_error, null);
		new AlertDialog.Builder(context)
				.setCancelable(false)
				.setView(dialogView)
				.setPositiveButton("Закрити", (dialog1, which) -> closeCallback.run())
				.show();
		ButterKnife.bind(this, dialogView);
		detailsTextView.setText(errorMessage);
	}

	@OnClick(showDetails)
	void onShowDetailsClick() {
		showDetailsButton.animate()
				.setDuration(150)
				.alpha(0)
				.start();
		detailsTextView.animate()
				.setStartDelay(150)
				.setDuration(150)
				.alpha(0.54f)
				.start();
	}

}