package ua.iyps.oopcoursework.presentation.table;

import java.util.List;

import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseView;

/**
 * Інтерфейс взаємодії Presenter з фрагментом
 */
public interface TableView extends BaseView {

	/**
	 * Прив'язує список до адаптера
	 */
	void bindList(List<AudioFile> items);

	/**
	 * Оновляє стан списку
	 */
	void updateList();

	/**
	 * Показує діалог про фонову задачу
	 */
	void showWaitDialog();

	/**
	 * Ховає діалог про фонову задачу
	 */
	void hideWaitDialog();

	/**
	 * Показує діалог про помилку читання
	 */
	void showErrorDialog(String message);

	/**
	 * Показує діалог редагування/видалення аудіофайлу
	 */
	void showItemDialog();

	/**
	 * Показує діалог підтвердження видалення файлу
	 */
	void showConfirmDeleteDialog();

	/**
	 * Показує діалог з пропозицією зберегти файл
	 */
	void showSaveSuggestionDialog(Runnable yes, Runnable no);

	/**
	 * Повертає бітрейт для пошуку
	 */
	Integer getBitrateForSearch();

	/**
	 * Повертає кодек для пошуку
	 */
	String getCodecForSearch();

}