package ua.iyps.oopcoursework.presentation.table;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;

/**
 * Адаптер спику (таблиці)
 */
public class TableAdapter extends RecyclerView.Adapter<TableAdapter.ViewHolder> implements View.OnClickListener {

	/**
	 * Список рядків
	 */
	private List<AudioFile> items;

	/**
	 * Presenter фрагмента
	 */
	private TablePresenter presenter;

	/**
	 * Конструктор по-замовчуванню
	 */
	public TableAdapter(TablePresenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Прив'язує список аудіофайлів до цього адаптера
	 */
	public void bind(List<AudioFile> itemsList) {
		this.items = itemsList;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View itemView = layoutInflater.inflate(R.layout.item_table, parent, false);
		return new ViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		AudioFile audioFile = items.get(position);
		holder.url.setText(audioFile.url);
		holder.extension.setText(audioFile.extension);
		holder.duration.setText(String.valueOf(audioFile.duration));
		holder.codec.setText(audioFile.codec);
		holder.hasKaraokeText.setText(transformBooleanToEmoji(audioFile.hasKaraokeText));
		holder.sampleRate.setText(String.valueOf(audioFile.sampleRate));
		holder.bitrate.setText(String.valueOf(audioFile.bitrate));
		holder.playerName.setText(audioFile.playerForOpen);
		holder.itemView.setTag(position);
		holder.itemView.setOnClickListener(this);
	}

	@Override
	public int getItemCount() {
		if(items != null) {
			return items.size();
		} else {
			return 0;
		}
	}

	@Override
	public void onClick(View v) {
		presenter.onItemClick((Integer) v.getTag());
	}

	/**
	 * Конвертує булівське значення у відповідний знак Емодзі (галочка або хрестик)
	 */
	private String transformBooleanToEmoji(boolean value) {
		if(value) {
			return String.valueOf(Character.toChars(10004));
		} else {
			return String.valueOf(Character.toChars(10006));
		}
	}

	static class ViewHolder extends RecyclerView.ViewHolder {

		@BindView(R.id.url)
		public TextView url;

		@BindView(R.id.extension)
		public TextView extension;

		@BindView(R.id.duration)
		public TextView duration;

		@BindView(R.id.codec)
		public TextView codec;

		@BindView(R.id.hasKaraokeText)
		public TextView hasKaraokeText;

		@BindView(R.id.sampleRate)
		public TextView sampleRate;

		@BindView(R.id.bitrate)
		public TextView bitrate;

		@BindView(R.id.playerName)
		public TextView playerName;

		public ViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
	}

}