package ua.iyps.oopcoursework.presentation.additem;

import android.text.TextUtils;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.utils.ValidatorHelper;

/**
 * Фрагмент, який відповідає за запис розширення
 */
@Layout(R.layout.fragment_add_ext)
public class ExtFragment extends BaseFragment implements ValidatorHelper.Validatable {

	@BindView(R.id.extField)
	EditText editText;

	AddItemContract contract;
	AudioFile audioFile;

	public ExtFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ValidatorHelper.bind(editText, this);
		if(audioFile.extension != null) {
			editText.setText(audioFile.extension);
		}
	}

	@Override
	public boolean validate() {
		String input = editText.getText().toString();
		if(!TextUtils.isEmpty(input) && input.length() < 5) {
			for(int i = 0; i < input.length(); i++) {
				char c = Character.toLowerCase(input.charAt(i));
				if(!((c >= 'a' & c <= 'z') | (c >= '0' & c <= '9'))) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onValid() {
		audioFile.extension = editText.getText().toString();
		contract.addFragment(new DurationFragment(contract));
		contract.setPageIndex(2);
	}

	@Override
	public void onError() {
		showError("Некоректно введене розширення файлу, перевірте написання та повторіть спробу.");
	}

	@Override
	public void onSilentLock() {
		contract.setPagingEnabled(false);
	}

	@Override
	public void onSilentUnlock() {
		contract.setPagingEnabled(true);
	}

	@OnClick(R.id.mp3)
	void onMp3Clicked() {
		editText.setText("mp3");
		editText.requestFocus();
	}

	@OnClick(R.id.wav)
	void onWavClicked() {
		editText.setText("wav");
		editText.requestFocus();
	}

	@OnClick(R.id.wma)
	void onWmaClicked() {
		editText.setText("wma");
		editText.requestFocus();
	}

	@OnClick(R.id.flac)
	void onFlacClicked() {
		editText.setText("flac");
		editText.requestFocus();
	}

	@OnClick(R.id.ogg)
	void onOggClicked() {
		editText.setText("ogg");
		editText.requestFocus();
	}

}