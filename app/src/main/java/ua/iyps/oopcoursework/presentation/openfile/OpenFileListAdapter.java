package ua.iyps.oopcoursework.presentation.openfile;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.iyps.oopcoursework.R;

/**
 * Адаптер списку файлів
 */
public class OpenFileListAdapter extends RecyclerView.Adapter<OpenFileListAdapter.ViewHolder> implements View.OnClickListener {

	/**
	 * Слухач подій від цього адаптера
	 */
	private OnItemSelectedListener listener;

	/**
	 * Список пунктів списку
	 */
	private List<Item> items;

	/**
	 * Конструктор по-замовчуванню
	 */
	public OpenFileListAdapter() {
		items = new ArrayList<>();
	}

	/**
	 * Очищає список
	 */
	public void clear() {
		items.clear();
		notifyDataSetChanged();
	}

	/**
	 * Додає пункт типу "папка"
	 * @param folderFile папка, яка відповідає цьому пункту
	 */
	public void addFolder(File folderFile) {
		items.add(new Item(folderFile, Item.Type.FOLDER));
		notifyItemRangeChanged(items.size(), 1);
	}

	/**
	 * Додає пункт типу "файл"
	 * @param file файл, який відповідає цьому пункту
	 */
	public void addFile(File file) {
		items.add(new Item(file, Item.Type.FILE));
		notifyItemRangeChanged(items.size(), 1);
	}

	/**
	 * Додає пункт типу "угору"
	 */
	public void addUp() {
		items.add(Item.up());
		notifyItemRangeChanged(items.size(), 1);
	}

	/**
	 * Встановлює слухач подій
	 */
	public void setOnItemSelectedListener(OnItemSelectedListener listener) {
		this.listener = listener;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View itemView = inflater.inflate(R.layout.item_file_open_list, parent, false);
		return new ViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		Item item = items.get(position);
		holder.icon.setImageResource(getIconId(item.type));
		holder.text.setText(item.name);
		holder.itemView.setTag(position);
		holder.itemView.setOnClickListener(this);
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	/**
	 * Повертає ідентифікатор ресурсу іконки за її типом
	 */
	private int getIconId(Item.Type type) {
		switch (type) {
			case FOLDER:
				return R.drawable.ic_folder;
			case UP:
				return R.drawable.ic_up;
			default:
				return R.drawable.ic_file;
		}
	}

	@Override
	public void onClick(View v) {
		int position = (int) v.getTag();
		Item item = items.get(position);
		notifyListener(item.type, item.file);
	}

	/**
	 * Сповіщає слухача про подію
	 */
	private void notifyListener(Item.Type type, File file) {
		if (listener != null) {
			listener.onItemSelected(type, file);
		}
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		@BindView(R.id.icon)
		ImageView icon;

		@BindView(R.id.caption)
		TextView text;

		public ViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
	}

	/**
	 * Пункт списку
	 */
	public static class Item {

		public enum Type {
			UP, FOLDER, FILE
		}

		public Type type;
		public String name;
		public File file;

		public Item() {

		}

		public Item(File file, Type type) {
			this.type = type;
			this.file = file;
			this.name = file.getName();
		}

		public static Item up() {
			Item item = new Item();
			item.type = Type.UP;
			item.name = "(Угору)";
			return item;
		}

	}

	/**
	 * Інтерфейс слухача
	 */
	public interface OnItemSelectedListener {

		/**
		 * Викликається, коли відбувся клік на пункт списку
		 * @param type тип пункту
		 * @param file файл, який асоціюється з цим пуктом
		 */
		void onItemSelected(Item.Type type, File file);
	}

}