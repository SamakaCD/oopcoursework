package ua.iyps.oopcoursework.presentation.empty;

import java.io.File;

import javax.inject.Inject;

import rx.functions.Action1;
import teamvoy.fragmentpico.FragmentHolder;
import ua.iyps.oopcoursework.Constants;
import ua.iyps.oopcoursework.presentation.common.BasePresenter;
import ua.iyps.oopcoursework.presentation.openfile.OpenFileFragment;
import ua.iyps.oopcoursework.presentation.table.TableFragment;

/**
 * Presenter фрагмента порожнього стану
 */
public class EmptyPresenter extends BasePresenter {

	/**
	 * Контейнер фрагментів
	 */
	@Inject
	FragmentHolder fragmentHolder;

	/**
	 * Конструктор ін'єкції
	 */
	@Inject
	public EmptyPresenter() { }

	/**
	 * Викликається при потребі відкрити файл
	 */
	public void onOpenFile() {
		fragmentHolder.add(new OpenFileFragment()
				.putArgument(Constants.ARG_OPEN_FILE_CALLBACK, onFileOpened));
	}

	/**
	 * Викликається при потребі створити файл
	 */
	public void onCreateNewFile() {
		fragmentHolder.clear();
		fragmentHolder.add(new TableFragment());
	}

	/**
	 * Викликається, коли користувач обрав файл для відкриття
	 */
	private Action1<File> onFileOpened = file -> {
		fragmentHolder.clear();
		fragmentHolder.add(new TableFragment()
				.putArgument(Constants.ARG_FILE, file));
	};

}