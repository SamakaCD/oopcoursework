package ua.iyps.oopcoursework.presentation.additem;

import android.text.TextUtils;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.utils.ValidatorHelper;

/**
 * Фрагмент, який відповідає за запис кодека
 */
@Layout(R.layout.fragment_add_codec)
public class CodecFragment extends BaseFragment implements ValidatorHelper.Validatable {

	@BindView(R.id.field)
	EditText editText;

	AddItemContract contract;
	AudioFile audioFile;

	public CodecFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ValidatorHelper.bind(editText, this);
		if(audioFile.codec != null) {
			editText.setText(audioFile.codec);
		}
	}

	@Override
	public boolean validate() {
		String input = editText.getText().toString();
		return !TextUtils.isEmpty(input) & input.length() < 255;
	}

	@Override
	public void onValid() {
		audioFile.codec = editText.getText().toString();
		contract.addFragment(new HasKaraokeTextFragment(contract));
		contract.setPageIndex(4);
	}

	@Override
	public void onError() {
		if(editText.getText().toString().length() >= 255) {
			showError("Назва кодеку занадто довга.");
		} else {
			showError("Некоректно введена назва кодеку, перевірте написання та повторіть спробу.");
		}
	}

	@Override
	public void onSilentLock() {
		contract.setPagingEnabled(false);
	}

	@Override
	public void onSilentUnlock() {
		contract.setPagingEnabled(true);
	}

	@OnClick(R.id.mp3)
	void onMp3Click() {
		editText.setText("MP3");
		editText.requestFocus();
	}

	@OnClick(R.id.mp4)
	void onMp4Click() {
		editText.setText("MP4");
		editText.requestFocus();
	}

	@OnClick(R.id.aac)
	void onAacClick() {
		editText.setText("AAC");
		editText.requestFocus();
	}

	@OnClick(R.id.vorbis)
	void onVorbisClick() {
		editText.setText("Vorbis");
		editText.requestFocus();
	}

	@OnClick(R.id.amr)
	void onAmrlick() {
		editText.setText("AMR");
		editText.requestFocus();
	}

}