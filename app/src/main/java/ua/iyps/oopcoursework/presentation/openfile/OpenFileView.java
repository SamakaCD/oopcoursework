package ua.iyps.oopcoursework.presentation.openfile;

import java.io.File;

/**
 * Інтерфейс взаємодії Presenter з фрагментом
 */
public interface OpenFileView {

	/**
	 * Встановлює шлях до поточної папки
	 * @param path шлях до поточної папки
	 */
	void setCurrentFolder(String path);

	/**
	 * Очищає список
	 */
	void clearList();

	/**
	 * Додає пункт "угору"
	 */
	void addUp();

	/**
	 * Додає пункт "папка"
	 * @param folder папка, якій відповідатиме пункт
	 */
	void addFolder(File folder);

	/**
	 * Додає пункт "файл"
	 * @param file файл, якому відповідатиме пункт
	 */
	void addFile(File file);

	/**
	 * Сповіщає фрагмент про те, що файл вибраний
	 */
	void fileSelected();

}