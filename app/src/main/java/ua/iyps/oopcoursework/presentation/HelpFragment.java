package ua.iyps.oopcoursework.presentation;

import android.view.View;

import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;

@Layout(R.layout.fragment_help)
public class HelpFragment extends BaseFragment {

	@OnClick(R.id.workWithFilesTitle)
	void onWorkWithFilesClick() {
		getView().findViewById(R.id.workWithFilesContent).setVisibility(View.VISIBLE);
	}

	@OnClick(R.id.createTableTitle)
	void onCreateTableTitle() {
		getView().findViewById(R.id.createTableContent).setVisibility(View.VISIBLE);
	}

	@OnClick(R.id.openFileTitle)
	void onOpenFileClick() {
		getView().findViewById(R.id.openFileContent).setVisibility(View.VISIBLE);
	}

	@OnClick(R.id.addItemTitle)
	void onAddItemClick() {
		getView().findViewById(R.id.addItemContent).setVisibility(View.VISIBLE);
	}

	@OnClick(R.id.editTitle)
	void onEditClick() {
		getView().findViewById(R.id.editContent).setVisibility(View.VISIBLE);
	}

	@OnClick(R.id.saveTitle)
	void onSaveClick() {
		getView().findViewById(R.id.saveContent).setVisibility(View.VISIBLE);
	}

	@OnClick(R.id.closeButton)
	void onClose() {

	}

}