package ua.iyps.oopcoursework.presentation.savefile;

import java.io.File;

import ua.iyps.oopcoursework.presentation.common.BaseView;

/**
 * Інтерфейс взаємодії Presenter з фрагментом
 */
public interface SaveFileView extends BaseView {

	/**
	 * Встановлює шлях до поточної папки
	 * @param path шлях до поточної папки
	 */
	void setCurrentFolder(String path);

	/**
	 * Очищає список
	 */
	void clearList();

	/**
	 * Додає пункт "угору"
	 */
	void addUp();

	/**
	 * Додає пункт "папка"
	 * @param folder папка, якій відповідатиме пункт
	 */
	void addFolder(File folder);

	/**
	 * Додає пункт "файл"
	 * @param file файл, якому відповідатиме пункт
	 */
	void addFile(File file);

	/**
	 * Закриває фрагмент
	 */
	void close();

	/**
	 * Повертає введену користувачем назву файлу
	 */
	String getFileName();

}