package ua.iyps.oopcoursework.presentation;

import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;

@Layout(R.layout.fragment_about)
public class AboutFragment extends BaseFragment { }