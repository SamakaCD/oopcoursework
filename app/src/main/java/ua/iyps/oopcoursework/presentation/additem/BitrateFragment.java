package ua.iyps.oopcoursework.presentation.additem;

import android.text.TextUtils;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import ua.iyps.oopcoursework.R;
import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.presentation.common.BaseFragment;
import ua.iyps.oopcoursework.presentation.common.Layout;
import ua.iyps.oopcoursework.utils.ValidatorHelper;

/**
 * Фрагмент, який відповідає за запис бітрейту
 */
@Layout(R.layout.fragment_add_bitrate)
public class BitrateFragment extends BaseFragment implements ValidatorHelper.Validatable {

	@BindView(R.id.field)
	EditText editText;

	AddItemContract contract;
	AudioFile audioFile;

	public BitrateFragment(AddItemContract contract) {
		this.contract = contract;
		this.audioFile = contract.getAudioFile();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ValidatorHelper.bind(editText, this);
		if(audioFile.bitrate != null) {
			editText.setText(String.valueOf(audioFile.bitrate));
		}
	}

	@Override
	public boolean validate() {
		String input = editText.getText().toString();
		try {
			Integer.parseInt(input);

			if(!TextUtils.isEmpty(input) && input.length() > 4) {
				return false;
			}

			return true;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void onValid() {
		audioFile.bitrate = Integer.parseInt(editText.getText().toString());
		contract.addFragment(new PlayerFragment(contract));
		contract.setPageIndex(7);
	}

	@Override
	public void onError() {
		if(editText.getText().toString().length() > 4) {
			showError("Введений бітрейт надто великий.");
		} else {
			showError("Введений некоректний бітрейт, перевірте написання та повторіть спробу.");
		}
	}

	@Override
	public void onSilentLock() {
		contract.setPagingEnabled(false);
	}

	@Override
	public void onSilentUnlock() {
		contract.setPagingEnabled(true);
	}

	@OnClick(R.id.bitrate1)
	void onSelectBitrate1() {
		editText.setText("128");
		editText.requestFocus();
	}

	@OnClick(R.id.bitrate2)
	void onSelectBitrate2() {
		editText.setText("160");
		editText.requestFocus();
	}

	@OnClick(R.id.bitrate3)
	void onSelectBitrate3() {
		editText.setText("192");
		editText.requestFocus();
	}

	@OnClick(R.id.bitrate4)
	void onSelectBitrate4() {
		editText.setText("320");
		editText.requestFocus();
	}

	@OnClick(R.id.bitrate5)
	void onSelectBitrate5() {
		editText.setText("256");
		editText.requestFocus();
	}

}