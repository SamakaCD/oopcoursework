package ua.iyps.oopcoursework.presentation.custom;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Snackbar {

	public static final int LENGTH_SHORT = 1750;
	public static final int LENGTH_LONG = 2500;
	public static final int LENGTH_INDEFINITE = 0;

	private static final String TAG_TEXT = "snackbar_text";

	private ViewGroup container;
	private String text;
	private int duration;
	private int backgroundColor;
	private int textColor;
	private int snackbarWidth;

	public static Snackbar make(View view, String text, int duration) {
		Snackbar snackbar = new Snackbar();
		snackbar.container = findSuitableParent(view);
		snackbar.text = text;
		snackbar.duration = duration;
		return snackbar;
	}

	public static Snackbar make(View view, int stirngResId, int duration) {
		return make(view, view.getResources().getString(stirngResId), duration);
	}

	public Snackbar() {
		backgroundColor = 0xFF323232;
		textColor = 0xFFFFFFFF;
	}

	public void show() {
		removeAvailableSnackbars(container);
		View snackbarView = createView();
		container.addView(snackbarView, createLayoutParamsForSnackbarLayout());
		animateSnackbarShow(snackbarView);
		if (duration > 0) {
			snackbarView.postDelayed(() -> animateSnackbarHide(snackbarView), duration);
		}
	}

	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public void setTextColor(int textColor) {
		this.textColor = textColor;
	}

	private View createView() {
		Context context = container.getContext();
		ViewGroup layout = createSnackbarLayout(context);
		View textView = createTextView(context);
		layout.addView(textView);
		return layout;
	}

	private ViewGroup createSnackbarLayout(Context context) {
		SnackbarLayout layout = new SnackbarLayout(context);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.setBackgroundColor(backgroundColor);
		layout.setPadding(dp(24), dp(14), dp(24), dp(14));

		if (container.getWidth() != 0 & container.getWidth() > dp(568)) {
			snackbarWidth = dp(568);
		}

		if (Build.VERSION.SDK_INT >= 21) {
			layout.setElevation(dp(2));
		}

		return layout;
	}

	private ViewGroup.LayoutParams createLayoutParamsForSnackbarLayout() {
		int snackbarWidth = getSnackbarWidthForLayoutParams();
		ViewGroup.LayoutParams layoutParams;

		if (container instanceof FrameLayout) {
			layoutParams = createFrameLayoutParams(snackbarWidth);
		} else if (container instanceof RelativeLayout) {
			layoutParams = createRelativeLayoutParams(snackbarWidth);
		} else {
			layoutParams = new ViewGroup.LayoutParams(snackbarWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
		}

		return layoutParams;
	}

	private View createTextView(Context context) {
		TextView textView = new TextView(context);
		textView.setTextColor(textColor);
		textView.setText(text);
		textView.setTag(TAG_TEXT);
		return textView;
	}

	private int dp(int dp) {
		return (int) (container.getResources().getDisplayMetrics().density * dp);
	}

	private int getSnackbarWidthForLayoutParams() {
		if (snackbarWidth == 0) {
			return ViewGroup.LayoutParams.MATCH_PARENT;
		} else {
			return snackbarWidth;
		}
	}

	private ViewGroup.LayoutParams createFrameLayoutParams(int snackbarWidth) {
		return new FrameLayout.LayoutParams(snackbarWidth,
				ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
	}

	private ViewGroup.LayoutParams createRelativeLayoutParams(int snackbarWidth) {
		RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(
				snackbarWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
		relativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		relativeLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		return relativeLayoutParams;
	}

	private void animateSnackbarShow(View snackbarView) {
		snackbarView.setVisibility(View.INVISIBLE);
		snackbarView.post(() -> {
			setupSnackBarLayoutPaddings(snackbarView);
			snackbarView.setVisibility(View.VISIBLE);
			snackbarView.setTranslationY(snackbarView.getHeight());
			snackbarView.animate()
					.setDuration(300)
					.setInterpolator(new LinearOutSlowInInterpolator())
					.translationY(0)
					.start();
		});

		TextView textView = (TextView) snackbarView.findViewWithTag(TAG_TEXT);
		textView.setAlpha(0);
		textView.animate()
				.setStartDelay(150)
				.setDuration(150)
				.alpha(1)
				.start();
	}

	private void animateSnackbarHide(View snackbarView) {
		snackbarView.animate()
				.setDuration(200)
				.setInterpolator(new FastOutLinearInInterpolator())
				.translationY(snackbarView.getHeight())
				.withEndAction(() -> removeSnackbarViewFromParent(snackbarView))
				.start();

		TextView textView = (TextView) snackbarView.findViewWithTag(TAG_TEXT);
		textView.animate()
				.setDuration(200)
				.alpha(0)
				.start();
	}

	private void removeSnackbarViewFromParent(View snackbarView) {
		ViewGroup parent = (ViewGroup) snackbarView.getParent();
		parent.removeView(snackbarView);
	}

	private void setupSnackBarLayoutPaddings(View snackbarView) {
		TextView textView = (TextView) snackbarView.findViewWithTag(TAG_TEXT);
		if (textView.getLineCount() > 1) {
			snackbarView.setPadding(dp(24), dp(24), dp(24), dp(24));
		} else {
			snackbarView.setPadding(dp(24), dp(14), dp(24), dp(14));
		}
	}

	private static ViewGroup findSuitableParent(View view) {
		do {
			if (view instanceof FrameLayout) {
				return (FrameLayout) view;
			} else if (view instanceof RelativeLayout) {
				return (RelativeLayout) view;
			} else {
				view = (View) view.getParent();
			}
		} while (view != null);

		return null;
	}

	private static void removeAvailableSnackbars(ViewGroup container) {
		for (int i = 0; i < container.getChildCount(); i++) {
			View child = container.getChildAt(i);
			if(child instanceof SnackbarLayout) {
				child.setVisibility(View.GONE);
			}
		}
	}

	private static class SnackbarLayout extends LinearLayout {
		public SnackbarLayout(Context context) {
			super(context);
		}
	}

}