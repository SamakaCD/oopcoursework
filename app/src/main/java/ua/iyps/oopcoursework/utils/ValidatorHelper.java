package ua.iyps.oopcoursework.utils;

import android.app.Activity;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import ua.iyps.oopcoursework.presentation.custom.TextWatcherAdapter;

public class ValidatorHelper {

	public static void bind(EditText editText, Validatable validatable) {
		editText.setOnEditorActionListener((v, actionId, event) -> {
			if(actionId == EditorInfo.IME_ACTION_NEXT) {
				Utils.hideKeyboard((Activity) editText.getContext());
				validate(validatable);
			}
			return false;
		});

		editText.setOnKeyListener((v, keyCode, event) -> {
			if(keyCode == KeyEvent.KEYCODE_ENTER & event.getAction() == KeyEvent.ACTION_UP) {
				Utils.hideKeyboard((Activity) editText.getContext());
				validate(validatable);
			}
			return false;
		});

		editText.addTextChangedListener(new TextWatcherAdapter() {
			@Override
			public void afterTextChanged(Editable s) {
				if(validatable.validate()) {
					validatable.onSilentUnlock();
				} else {
					validatable.onSilentLock();
				}
			}
		});
	}

	private static void validate(Validatable validatable) {
		if(validatable.validate()) {
			validatable.onValid();
		} else {
			validatable.onError();
		}
	}

	public interface Validatable {

		boolean validate();
		void onValid();
		void onError();
		void onSilentLock();
		void onSilentUnlock();

	}

}