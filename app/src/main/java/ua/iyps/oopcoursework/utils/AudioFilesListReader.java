package ua.iyps.oopcoursework.utils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.exception.FileNotFoundException;

public class AudioFilesListReader {

	private static final String CHARSET = "windows-1251";

	public List<AudioFile> read(File listFile) throws Exception {
		if(!listFile.exists()) {
			throw new FileNotFoundException(listFile.getName());
		}

		char[] chars = byteArrayToChars(readFile(listFile), CHARSET);
		AudioFilesListParser parser = new AudioFilesListParser();
		return parser.parse(chars);
	}

	private byte[] readFile(File file) throws Exception {
		DataInputStream stream = new DataInputStream(new FileInputStream(file));
		byte[] buffer = new byte[(int) file.length()];
		stream.readFully(buffer);
		return buffer;
	}

	private char[] byteArrayToChars(byte[] buffer, String charset) throws Exception {
		String string = new String(buffer, charset);
		return string.toCharArray();
	}

}