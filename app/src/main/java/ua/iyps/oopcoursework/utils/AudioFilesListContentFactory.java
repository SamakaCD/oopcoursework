package ua.iyps.oopcoursework.utils;

import java.util.List;

import ua.iyps.oopcoursework.entity.AudioFile;

public class AudioFilesListContentFactory {

	public String convert(List<AudioFile> audioFiles) {
		StringBuilder builder = new StringBuilder();
		for(AudioFile audioFile : audioFiles) {
			appendUrl(builder, audioFile);
			builder.append(" ");
			appendExtension(builder, audioFile);
			builder.append(" ");
			appendDuration(builder, audioFile);
			builder.append(" ");
			appendCodec(builder, audioFile);
			builder.append(" ");
			appendHasKaraokeText(builder, audioFile);
			builder.append(" ");
			appendSampleRate(builder, audioFile);
			builder.append(" ");
			appendBitrate(builder, audioFile);
			builder.append(" ");
			appendPlayerName(builder, audioFile);
			builder.append("\r\n");
		}
		return builder.toString();
	}

	private void appendUrl(StringBuilder builder, AudioFile audioFile) {
		appendQuoteIfNeed(builder, audioFile.url);
		builder.append(audioFile.url);
		appendQuoteIfNeed(builder, audioFile.url);
	}

	private void appendExtension(StringBuilder builder, AudioFile audioFile) {
		appendQuoteIfNeed(builder, audioFile.extension);
		builder.append(audioFile.extension);
		appendQuoteIfNeed(builder, audioFile.extension);
	}

	private void appendDuration(StringBuilder builder, AudioFile audioFile) {
		builder.append(audioFile.duration);
	}

	private void appendCodec(StringBuilder builder, AudioFile audioFile) {
		appendQuoteIfNeed(builder, audioFile.codec);
		builder.append(audioFile.codec);
		appendQuoteIfNeed(builder, audioFile.codec);
	}

	private void appendHasKaraokeText(StringBuilder builder, AudioFile audioFile) {
		builder.append(audioFile.hasKaraokeText ? "+" : "-");
	}

	private void appendSampleRate(StringBuilder builder, AudioFile audioFile) {
		builder.append(audioFile.sampleRate);
	}

	private void appendBitrate(StringBuilder builder, AudioFile audioFile) {
		builder.append(audioFile.bitrate);
	}

	private void appendPlayerName(StringBuilder builder, AudioFile audioFile) {
		appendQuoteIfNeed(builder, audioFile.playerForOpen);
		builder.append(audioFile.playerForOpen);
		appendQuoteIfNeed(builder, audioFile.playerForOpen);
	}

	private void appendQuoteIfNeed(StringBuilder buffer, String data) {
		if(data.contains(" ")) {
			buffer.append("\"");
		}
	}

}