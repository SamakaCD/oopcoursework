package ua.iyps.oopcoursework.utils;

import java.util.ArrayList;
import java.util.List;

import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.exception.ParseException;
import ua.iyps.oopcoursework.exception.TokenTooLongException;

public class AudioFilesListParser {

	private static final int MAX_STR_TOKEN_LENGTH = 256;
	private static final int MAX_INT_TOKEN_LENGTH = 9;
	private static final char[] SPLITTERS = {
			' ', '\r', '\n', '\t'
	};

	private List<AudioFile> audioFileList;
	private char[] chars;
	private int pos;

	public AudioFilesListParser() {
		this.audioFileList = new ArrayList<>();
	}

	public List<AudioFile> parse(char[] chars) {
		this.chars = chars;
		while (true) {
			skipSplitters();
			if (pos == chars.length) {
				break;
			}
			audioFileList.add(parseAudioFile());
			if (pos == chars.length) {
				break;
			}
		}
		return audioFileList;
	}

	private AudioFile parseAudioFile() {
		AudioFile audioFile = new AudioFile();
		skipSplitters();
		audioFile.url = readMaybeQuotedStringToken();
		skipSplitters();
		audioFile.extension = readMaybeQuotedStringToken();
		skipSplitters();
		audioFile.duration = readInt();
		skipSplitters();
		audioFile.codec = readMaybeQuotedStringToken();
		skipSplitters();
		audioFile.hasKaraokeText = readBoolean();
		skipSplitters();
		audioFile.sampleRate = readInt();
		skipSplitters();
		audioFile.bitrate = readInt();
		skipSplitters();
		audioFile.playerForOpen = readMaybeQuotedStringToken();
		skipSplitters();
		return audioFile;
	}

	private void skipSplitters() {
		while (true) {
			if (isEnd()) {
				break;
			}

			if (isSplitter(chars[pos])) {
				pos++;
			} else {
				break;
			}
		}
	}

	private String readMaybeQuotedStringToken() {
		if (chars[pos] == '"') {
			return readQuotedStringToken();
		} else {
			return readStringToken();
		}
	}

	private String readStringToken() {
		StringBuilder builder = new StringBuilder();
		int localPos = 0;
		while (true) {
			if (isEnd()) {
				break;
			}

			char c = chars[pos];

			if (localPos >= MAX_STR_TOKEN_LENGTH) {
				throw new TokenTooLongException("Занадто довгий маркер. Максимально допустима довжина - "
						+ MAX_STR_TOKEN_LENGTH);
			}

			if (isSplitter(c)) {
				break;
			}

			builder.append(c);
			localPos++;
			pos++;
		}
		return builder.toString();
	}

	private String readQuotedStringToken() {
		assertArrayBounds(chars, pos);
		StringBuilder builder = new StringBuilder();
		pos++;
		int localPos = 1;
		while (true) {
			if (isEnd()) {
				break;
			}

			char c = chars[pos];

			if (localPos >= MAX_STR_TOKEN_LENGTH) {
				throw new TokenTooLongException("Занадто довгий маркер. Максимально допустима довжина - "
						+ MAX_STR_TOKEN_LENGTH);
			}

			if (c == '"') {
				pos++;
				break;
			}

			builder.append(c);
			localPos++;
			pos++;
		}
		return builder.toString();
	}

	private int readInt() {
		StringBuilder builder = new StringBuilder();
		int localPos = 0;
		while (true) {
			if (isEnd()) {
				break;
			}

			char c = chars[pos];
			if (localPos >= MAX_INT_TOKEN_LENGTH) {
				throw new TokenTooLongException("Занадто довгий маркер. Максимально допустима довжина - "
						+ MAX_INT_TOKEN_LENGTH);
			}

			if (isSplitter(c)) {
				break;
			}

			if (!Character.isDigit(c)) {
				throw new ParseException("Некоректна цифра: " + c);
			}

			builder.append(c);
			localPos++;
			pos++;
		}
		return Integer.valueOf(builder.toString());
	}


	private boolean readBoolean() {
		assertArrayBounds(chars, pos);
		if (chars[pos] == '+') {
			pos++;
			return true;
		} else if (chars[pos] == '-') {
			pos++;
			return false;
		} else {
			throw new ParseException("Некоректний знак: " + chars[pos]);
		}
	}

	private boolean isSplitter(char c) {
		for (char splitter : SPLITTERS) {
			if (splitter == c) {
				return true;
			}
		}

		return false;
	}

	private void assertArrayBounds(char[] chars, int position) {
		if (position >= chars.length) {
			throw new ParseException();
		}
	}

	private boolean isEnd() {
		return pos == chars.length;
	}

}