package ua.iyps.oopcoursework.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import ua.iyps.oopcoursework.entity.AudioFile;
import ua.iyps.oopcoursework.exception.FileNotFoundException;

public class AudioFileListWriter {

	private static final String CHARSET = "windows-1251";

	public void write(File file, List<AudioFile> audioFiles) throws Exception {
		boolean isFileCreated = true;
		if(!file.exists()) {
			isFileCreated = file.createNewFile();
		}

		if(!isFileCreated) {
			throw new FileNotFoundException("Виникла помилка під час створення файлу.");
		}

		AudioFilesListContentFactory contentFactory = new AudioFilesListContentFactory();
		String content = contentFactory.convert(audioFiles);
		byte[] buffer = content.getBytes(CHARSET);
		FileOutputStream stream = new FileOutputStream(file);
		stream.write(buffer);
		stream.close();
	}

}