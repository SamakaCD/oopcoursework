package ua.iyps.oopcoursework.di;

import teamvoy.fragmentpico.FragmentHolder;

/**
 * Клас, у якому зберігається компонента для ін'єкції залежностей
 */
public class Injector {

	private static AppComponent appComponent;

	/**
	 * Виконує побудову графа залежностей
	 */
	public static void build(FragmentHolder fragmentHolder) {
		appComponent = DaggerAppComponent.builder()
				.appModule(new AppModule(fragmentHolder))
				.build();
	}

	/**
	 * Повертає побудовану компоненту графа залежностей
	 */
	public static AppComponent getAppComponent() {
		return appComponent;
	}

}