package ua.iyps.oopcoursework.di;

import javax.inject.Singleton;

import dagger.Component;
import ua.iyps.oopcoursework.presentation.additem.AddItemFragment;
import ua.iyps.oopcoursework.presentation.empty.EmptyFragment;
import ua.iyps.oopcoursework.presentation.openfile.OpenFileFragment;
import ua.iyps.oopcoursework.presentation.savefile.SaveFileFragment;
import ua.iyps.oopcoursework.presentation.table.TableFragment;

/**
 * Компонента для виконання ін'єкції залежностей
 */
@Singleton
@Component(modules = { AppModule.class })
public interface AppComponent {

	void inject(EmptyFragment emptyFragment);
	void inject(OpenFileFragment openFileFragment);
	void inject(TableFragment tableFragment);
	void inject(AddItemFragment addItemFragment);
	void inject(SaveFileFragment saveFileFragment);

}