package ua.iyps.oopcoursework.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import teamvoy.fragmentpico.FragmentHolder;

/**
 * Модуль залежностей
 */
@Module
public class AppModule {

	FragmentHolder fragmentHolder;

	public AppModule(FragmentHolder fragmentHolder) {
		this.fragmentHolder = fragmentHolder;
	}

	@Provides
	@Singleton
	FragmentHolder provideFragmentHolder() {
		return fragmentHolder;
	}

}