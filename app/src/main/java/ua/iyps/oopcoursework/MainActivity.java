package ua.iyps.oopcoursework;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

import teamvoy.fragmentpico.BackState;
import teamvoy.fragmentpico.FragmentHolder;
import ua.iyps.oopcoursework.di.Injector;
import ua.iyps.oopcoursework.presentation.custom.fragment.AnimatedFragmentViewAttacher;
import ua.iyps.oopcoursework.presentation.empty.EmptyFragment;

/**
 * Головна форма програми
 */
public class MainActivity extends Activity {

	/**
	 *  Контейнер фрагментів
	 */
	FragmentHolder fragmentHolder;

	/**
	 * Обробник подій для приховування SystemUI
	 */
	Handler handler;

	/**
	 * Викликається при створенні форми програми
	 * @param savedInstanceState містить збережений стан програми (не використовується)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		handler = new Handler();
		handler.post(hideStatusBarRunnable);
		ViewGroup rootView = (ViewGroup) findViewById(R.id.root);
		setupFragmentHolder(rootView);
		Injector.build(fragmentHolder);
		fragmentHolder.add(new EmptyFragment());
	}

	/**
	 * Проводить установку контейнера фрагментів
	 * @param rootView корінний елемент амкету
	 */
	private void setupFragmentHolder(ViewGroup rootView) {
		AnimatedFragmentViewAttacher attacher
				= new AnimatedFragmentViewAttacher(rootView, Color.WHITE);
		fragmentHolder = new FragmentHolder(attacher);
	}

	/**
	 * Викликається, коли користувач натиснув кнопку "назад"
	 */
	@Override
	public void onBackPressed() {
		if (fragmentHolder.back() == BackState.LAST_FRAGMENT) {
			super.onBackPressed();
		}
	}

	/**
	 * Задача приховування SystemUI
	 */
	Runnable hideStatusBarRunnable = new Runnable() {
		@Override
		public void run() {
			hideStatusBar();
			handler.postDelayed(hideStatusBarRunnable, 2000);
		}
	};

	/**
	 * Приховує SystemUI
	 */
	private void hideStatusBar() {
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_IMMERSIVE);
	}

	/**
	 * Приховує SystemUI
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE
							| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
							| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_FULLSCREEN
							| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		}
	}

}