package ua.iyps.oopcoursework;

public class Constants {

	/**
	 * Колбек відкриття файлу
	 */
	public static final String ARG_OPEN_FILE_CALLBACK = "callback";

	/**
	 * Колбек збереження файлу
	 */
	public static final String ARG_SAVE_FILE_CALLBACK = "callback";

	/**
	 * Файл для відкриття
	 */
	public static final String ARG_FILE = "file";

	/**
	 * Коллбек, який викличеться при завершенні додавання аудіофайлу
	 */
	public static final String ARG_ADD_ITEM_CALLBACK = "add_item_callback";

	/**
	 * Аудіофайл, який редагується
	 */
	public static final String ARG_EDIT_ITEM = "edit_item";

	/**
	 * Колбек, який викличеться після завершення редагування
	 */
	public static final String ARG_EDIT_FINISHED_CALLBACK = "edit_finished_callback";

}