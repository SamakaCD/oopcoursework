package ua.iyps.oopcoursework.exception;

/**
 * Виключення, яке відбувається при помилці парсингу файлу, який відкривається
 */
public class ParseException extends RuntimeException {

	public ParseException() {
		super();
	}

	public ParseException(String message) {
		super(message);
	}
}