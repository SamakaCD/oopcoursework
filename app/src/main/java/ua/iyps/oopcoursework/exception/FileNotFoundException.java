package ua.iyps.oopcoursework.exception;

/**
 * Виключення, яке використовується, коли поточний файл не знайдено
 */
public class FileNotFoundException extends RuntimeException {

	public FileNotFoundException(String fileName) {
		super("Файлу з назвою \"" + fileName + "\" не знайдено.");
	}

}