package ua.iyps.oopcoursework.exception;

/**
 * Виключення використовується при помилці парсингу, коли зчитувана стрічка перевищує ліміт знаків
 */
public class TokenTooLongException extends ParseException {
	public TokenTooLongException(String message) {
		super(message);
	}
}