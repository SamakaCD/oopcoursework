package teamvoy.fragmentpico;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

public class BackStack {

	private Stack<BackStackRecord> mStack;

	public BackStack() {
		mStack = new Stack<>();
	}

	public Fragment add(Fragment fragment, String tag) {
		BackStackRecord record = new BackStackRecord();
		record.fragment = fragment;
		record.tag = tag;

		if(tag != null) {
			filterStackByTag(tag);
		}

		mStack.push(record);

		if(mStack.size() > 1) {
			return mStack.get(mStack.size() - 2).fragment;
		} else {
			return null;
		}
	}

	public void clear() {
		for(BackStackRecord record : mStack) {
			record.fragment.onRequestDetach();
		}

		mStack.clear();
	}

	public BackResult back() {
		BackResult backResult = new BackResult();

		if(!mStack.empty()) {
			BackStackRecord record = mStack.peek();
			boolean wasDetached = record.fragment.requestDetach();
			backResult.fragment = record.fragment;

			if(wasDetached) {
				mStack.pop();
			} else {
				return backResult.withBackState(BackState.DECLINED);
			}

			if(mStack.size() == 0) {
				return backResult.withBackState(BackState.LAST_FRAGMENT);
			} else {
				backResult.replacement = mStack.peek().fragment;
				return backResult.withBackState(BackState.POP);
			}
		} else {
			return backResult.withBackState(BackState.EMPTY_STACK);
		}
	}

	public RemoveResult remove(Fragment fragment) {
		RemoveResult removeResult = new RemoveResult();

		int position = getFragmentPosition(fragment);
		if(position == -1) {
			return removeResult;
		}

		if(position == mStack.size() - 1) { // Top element of stack
			removeResult.isReplacementOnTop = true;
			removeResult.replacement = mStack.get(mStack.size() - 2).fragment;
		} else if(mStack.size() > 1) {
			removeResult.replacement = mStack.get(position + 1).fragment;
		}

		for(BackStackRecord record : mStack) {
			if(record.fragment == fragment) {
				mStack.remove(record);
			}
		}

		return removeResult;
	}

	public Collection<Fragment> getFragments() {
		Collection<Fragment> result = new ArrayList<>();
		for(BackStackRecord record : mStack) {
			result.add(record.fragment);
		}

		return result;
	}

	public int getFragmentPosition(Fragment fragment) {
		int position = -1;
		for(int i = 0; i < mStack.size(); i++) {
			if(mStack.get(i).fragment == fragment) {
				position = i;
			}
		}
		return position;
	}

	public int size() {
		return mStack.size();
	}

	private void filterStackByTag(String tag) {
		for(BackStackRecord record : mStack) {
			if(record.tag != null && record.tag.equals(tag)) {
				mStack.remove(record);
			}
		}
	}

	public static class BackResult {

		public Fragment fragment;
		public Fragment replacement;
		public BackState backState;

		public BackResult withBackState(BackState backState) {
			this.backState = backState;
			return this;
		}

	}

	public static class RemoveResult {

		public Fragment replacement;
		public boolean isReplacementOnTop;
		public boolean wasRemoved;

	}

	private static class BackStackRecord {

		public Fragment fragment;
		public String tag;

	}

}