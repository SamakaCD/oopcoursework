package teamvoy.fragmentpico;

import android.content.Context;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

public abstract class Fragment {

	private Context mContext;
	private View mView;
	private Map<String, Object> arguments;

	public Fragment() {
		arguments = new HashMap<>();
	}

	public Fragment putArgument(String key, Object value) {
		arguments.put(key, value);
		return this;
	}

	public Map<String, Object> getArguments() {
		return arguments;
	}

	public int getIntArgument(String key) {
		return (int) arguments.get(key);
	}

	public String getStringArgument(String key) {
		return (String) arguments.get(key);
	}

	public Object getArgument(String key) {
		return arguments.get(key);
	}

	public void updateContext(Context context) {
		mContext = context;
		onAttachContext(context);
	}

	public abstract View onCreateView();

	public void setupView() {
		mView = onCreateView();
	}

	public View getView() {
		return mView;
	}

	public void onAttachContext(Context context) {

	}

	public void onCreate() {

	}

	public void onPostCreate() {

	}

	public void onShow() {

	}

	public void onHide() {

	}

	protected Context getContext() {
		return mContext;
	}

	public boolean requestDetach() {
		boolean wasDetached = onRequestDetach();
		if(wasDetached) {
			onDestroy();
		}

		return wasDetached;
	}

	public boolean onRequestDetach() {
		return true;
	}

	public void onDestroy() {

	}

}