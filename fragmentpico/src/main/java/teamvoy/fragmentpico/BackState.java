package teamvoy.fragmentpico;

import android.app.Activity;

/**
 * Indicates result of removing Fragment from back stack
 */
public enum BackState {

	/**
	 * Indicates that top fragment of back stack successfully removed from back stack
	 */
	POP,

	/**
	 * Indicates that top fragment of back stack declined removing from back stack
	 */
	DECLINED,

	/**
	 * Indicates that top fragment of back stack successfully removed from back stack, but it was last fragment here.
	 * It can be used in method {@link Activity#onBackPressed()} implementation for checking Activity finish.
	 */
	LAST_FRAGMENT,

	/**
	 * Indicates that back stack is empty and nothing to remove and detach
	 */
	EMPTY_STACK

}