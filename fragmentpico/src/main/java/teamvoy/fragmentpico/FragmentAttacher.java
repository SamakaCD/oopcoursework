package teamvoy.fragmentpico;

import java.util.List;

/**
 * An abstract class which allows to attach fragment to container, which "linked" with FragmentHolder
 */
public abstract class FragmentAttacher {

	/**
	 * Called when {@link Fragment} should to be attached to container
	 * @param fragment Fragment for attaching
	 */
	public abstract void attach(Fragment fragment);

	public void replace(Fragment attachingFragment, List<Fragment> fragmentsForDetach) {
		attach(attachingFragment);
		for(Fragment fragment : fragmentsForDetach) {
			forceDetach(fragment);
		}
	}

	public boolean isTransitionInProgress() {
		return false;
	}

	/**
	 * Called when {@link Fragment} was showed after hiding
	 * @param fragment
	 */
	public abstract void show(Fragment fragment);

	/**
	 * Called when another {@link Fragment} will showed over current
	 * @param fragment
	 */
	public abstract void hide(Fragment fragment);

	/**
	 * Called when {@link Fragment} should to be detached to container
	 * @param fragment Fragment for detaching
	 */
	public abstract void detach(Fragment fragment);

	/**
	 * Called when {@link Fragment} should to be detached immediately
	 * @param fragment
	 */
	public abstract void forceDetach(Fragment fragment);

	/**
	 * Called when all Fragments should be detached from container
	 */
	public abstract void detachAll();

}