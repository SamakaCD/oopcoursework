package teamvoy.fragmentpico;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A virtual container, where there are transactions of fragments.
 * By default idea it linked with some {@link FragmentAttacher}, which attaches fragments to some container.
 */
public class FragmentHolder {

	private FragmentAttacher mFragmentAttacher;
	private BackStack mBackStack;
	private FragmentAttacherParams mPendingFragmentAttacherParams;
	private boolean mDetachLastFragment;

	public FragmentHolder() {
		mBackStack = new BackStack();
	}

	public FragmentHolder(FragmentAttacher fragmentAttacher) {
		this();
		mFragmentAttacher = fragmentAttacher;
	}

	public void setFragmentAttacher(FragmentAttacher fragmentAttacher) {
		mFragmentAttacher = fragmentAttacher;
	}

	public void setPendingFragmentAttacherParams(FragmentAttacherParams params) {
		mPendingFragmentAttacherParams = params;
	}

	/**
	 * Adds fragment to back stack and attaches it by current FragmentAttacher
	 * @param fragment
	 */
	public void add(Fragment fragment) {
		add(fragment, null);
	}

	/**
	 * Adds fragment to back stack and attaches it by current FragmentAttacher.
	 * All fragments with <b>tag</b> from parameters will be removed from back stack and detached by FragmentAttacher.
	 * @param fragment fragment for adding
	 * @param tag tag of fragment
	 */
	public void add(Fragment fragment, String tag) {
		setupPendingFragmentAttacherParams();
		mFragmentAttacher.attach(fragment);
		fragment.onCreate();

		Fragment previousFragment = mBackStack.add(fragment, tag);
		if(previousFragment != null) {
			previousFragment.onHide();
			mFragmentAttacher.hide(previousFragment);
		}

		fragment.onPostCreate();
		releasePendingFragmentAttacherParams();
	}

	public void replace(Fragment fragment) {
		setupPendingFragmentAttacherParams();
		mFragmentAttacher.replace(fragment, new ArrayList<>(mBackStack.getFragments()));
		fragment.onCreate();
		mBackStack.clear();
		mBackStack.add(fragment, null);
		fragment.onPostCreate();
		releasePendingFragmentAttacherParams();
	}

	/**
	 * Removes fragment from back stack and detaches it by FragmentAttacher
	 * @param fragment
	 */
	public void remove(Fragment fragment) {
		setupPendingFragmentAttacherParams();
		BackStack.RemoveResult removeResult = mBackStack.remove(fragment);
		mFragmentAttacher.detach(fragment);
		if(removeResult.isReplacementOnTop) {
			removeResult.replacement.onShow();
			mFragmentAttacher.show(removeResult.replacement);
		}
		releasePendingFragmentAttacherParams();
	}

	/**
	 * Removes top fragment of back stack and detaches by FragmentAttacher
	 * @return Result of detaching. See {@link com.teamvoy.fragmentpico.BackState}
	 */
	public BackState back() {
		if(mFragmentAttacher.isTransitionInProgress()) {
			return BackState.DECLINED;
		}

		setupPendingFragmentAttacherParams();
		BackStack.BackResult backResult = mBackStack.back();

		if(backResult.backState == BackState.DECLINED) {
			releasePendingFragmentAttacherParams();
			return BackState.DECLINED;
		}

		if(!mDetachLastFragment | backResult.backState != BackState.LAST_FRAGMENT) {
			mFragmentAttacher.detach(backResult.fragment);
		}

		if(backResult.replacement != null) {
			backResult.replacement.onShow();
			mFragmentAttacher.show(backResult.replacement);
		}
		releasePendingFragmentAttacherParams();
		return backResult.backState;
	}

	/**
	 * Gets all fragments from a back stack
	 * @return collection, which contains all fragments from a back stack
	 */
	public Collection<Fragment> getFragments() {
		return mBackStack.getFragments();
	}

	/**
	 * Sets whether a last fragment of back stack should be detached.
	 * It allows animate Activity finish without white blink.
	 * <b>false</b> by default.
	 * @param detachLastFragment true if detach last fragment of back
	 */
	public void setDetachLastFragment(boolean detachLastFragment) {
		mDetachLastFragment = detachLastFragment;
	}

	/**
	 * Clears back stack and detaches all views by FragmentAttacher
	 */
	public void clear() {
		mBackStack.clear();
		mFragmentAttacher.detachAll();
	}

	private void setupPendingFragmentAttacherParams() {
		if(mPendingFragmentAttacherParams != null) {
			mPendingFragmentAttacherParams.onSetupParams(mFragmentAttacher);
		}
	}

	private void releasePendingFragmentAttacherParams() {
		if(mPendingFragmentAttacherParams != null) {
			mPendingFragmentAttacherParams.onResetParams(mFragmentAttacher);
			mPendingFragmentAttacherParams = null;
		}
	}

}