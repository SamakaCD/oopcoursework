package teamvoy.fragmentpico;

public abstract class FragmentAttacherParams {

	public abstract void onSetupParams(FragmentAttacher fragmentAttacher);

	public abstract void onResetParams(FragmentAttacher fragmentAttacher);

}