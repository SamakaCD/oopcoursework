package teamvoy.fragmentpico;

import android.view.View;
import android.view.ViewGroup;

/**
 * Used for attaching Fragments to ViewGroup containers with default {@link ViewGroup.LayoutParams}
 */
public class FragmentViewAttacher extends FragmentAttacher {

	private ViewGroup mParent;
	private boolean mHideUnderFragment;

	public FragmentViewAttacher(ViewGroup parent) {
		mParent = parent;
	}

	@Override
	public void attach(Fragment fragment) {
		fragment.updateContext(mParent.getContext());
		fragment.setupView();
		mParent.addView(fragment.getView());
	}

	@Override
	public void show(Fragment fragment) {
		fragment.getView().setVisibility(View.VISIBLE);
	}

	@Override
	public void hide(Fragment fragment) {
		if(mHideUnderFragment) {
			fragment.getView().setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void detach(Fragment fragment) {
		mParent.removeView(fragment.getView());
	}

	@Override
	public void forceDetach(Fragment fragment) {
		detach(fragment);
	}

	@Override
	public void detachAll() {
		mParent.removeAllViews();
	}

	public void setHideUnderFragment(boolean hideUnderFragment) {
		mHideUnderFragment = hideUnderFragment;
	}

}